###
Module dependencies.
###

require('newrelic')
require("coffee-script")

express = require("express") 
http = require("http") 
path = require('path')
routes = require("./routes")
api = require("./routes/api") # API Path


app = module.exports = express()

###
Configuration
###

# all environments
app.set "views", __dirname + "/views"
app.set "view engine", "jade"
#app.use express.logger('dev')
app.use express.bodyParser()
app.use express.methodOverride()
app.use express.static(path.join(__dirname, '/app'))
app.use app.router
app.locals.pretty = true # outputs jade to html readable



###
ROUTES
###

# Get Page Layout Template
app.get "/", routes.index
# Get Pages Not Requiring Login
app.get "/partials/:name", routes.partials
# redirect all others to the index (HTML5 history)
app.get "*", routes.index

# Start server
app.listen 22974, ->
  console.log "Express server listening on port 22974 - Launch Red Donor Main"