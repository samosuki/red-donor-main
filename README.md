# README #

Red Donor Main Site

### Quick Summary ###
Red Donor Main Site v 1.0

Main static website using HTML/CSS, Angular.js and Node.js. Other technologies used - Grunt, Bower, CoffeeScript 

### Usage ###

* For display and backup only

### Ownership ###

* Built by @samosuki for Red Donor.com