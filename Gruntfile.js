module.exports = function (grunt) {
  "use strict";
  
  // Project Configuration
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    meta: {
      version: '<%= pkg.version %>',
      banner: '<%= pkg.name %> - <%= pkg.version %> - ' +
              'Created: <%= grunt.template.today("dd-mm-yyyy") %> ' + 
              'Copyright (c) <%= grunt.template.today("yyyy") %> Red Donor ' +
              'Developed by Samora Reid. ' 
    },
    shell: {
      options: {
        stdout: true
      },
      selenium: {
        command: './selenium/start',
        options: {
          stdout: false,
          async: true
        }
      },
      protractor_install: {
        command: 'node ./node_modules/protractor/bin/install_selenium_standalone'
      },
      npm_install: {
        command: 'npm install'
      },
      bower_install: {
        command: 'node ./node_modules/bower/bin/bower install'
      },
    },
    protractor: {
      options: {
        keepAlive: false,
        configFile: "test/e2e/protractor.conf.js"
      },
      singlerun: {},
      auto: {
        keepAlive: true,
        options: {
          args: {
            seleniumPort: 4444
          }
        }
      }
    },
    // configuration for less (development & production)
    less: {
      development: {
        options: {
          paths: ["app/styles"]
        },
        files: {
          "app/styles/styles.css": "app/styles/styles.less"
        }
      },
      production: {
        options: {
          paths: ["app/styles"]
        },
        files: {
          "app/styles/styles.css": "app/styles/styles.less"
        }
      }
    },
    // configuration for autoprefixer of custom stylesheets
    autoprefixer: {
      options: {
          browsers: ['last 2 versions']
        },
        dist: {
          files: {
            'app/styles/styles.css': 'app/styles/styles.css'
          }
        }
     },
     // configuration for cssmin minification of custom stylesheets and vendor css
     cssmin: {
      add_banner: {
        options: {
          banner: '/* <%= meta.banner %> || Minify CSS Files: <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
        },
        files: {
          'app/styles/styles.min.css': ['app/vendor/bootstrap/css/bootstrap.min.css','app/styles/styles.css']
        }
      }
    },
    jade: {
      html: {
        files: {
          'views/partials/templates/': ['views/**/*.jade']
        },
        options: {
          client: false,
          //basePath: 'views/',
          pretty: true,
        }
      }
    },
    // configuration for grunt-angular-templates
    ngtemplates: {
      reddonor: { // matches the name of the angular module defined in app.js
        options: {
          base: "views"
        },
        src: "views/**/*.html",
        dest: "views/template-cache.js"
      }
    },
    ngmin: {
      controllers: {
        src: ['app/js/controllers/*.js'],
        dest: 'app/js/controllers.min.js'
      },
      directives: {
        src: ['app/js/directives/*.js'],
        dest: 'app/js/directives.min.js'
      }
    },
    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: ['app/js/**/*.js', 'app/vendor/bootstrap/js/bootstrap.min.js','<%= ngtemplates.reddonor.dest %>'],
        dest: 'app/assets/js/app.js'
      },
    },
    uglify: {
      options: {
        banner: '/* <%= meta.banner %> || Uglify Angular Files & Other JavaScript Files: <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          'app/assets/js/app.min.js': ['<%= concat.dist.dest %>']
        }
      },
    },
    /*rev: {
      files: {
        src: ['styles/styles.css']
      }
    },*/
    rev: {
      options: {
        encoding: 'utf8',
        algorithm: 'md5',
        length: 8
      },
      assets: {
        files: [{
          src: ['app/assets/js/app.min.js']
        }]
      }
    },
    forcemin: {
      src: [
        'app/assets/*.{js,css,html,ejs}','views/**/*.html'
      ]
    },
    karma: {
      unit: {
        configFile: 'test/unit/karma.conf.js',
        autoWatch: false,
        singleRun: true
      },
      unit_auto: {
        configFile: 'test/unit/karma.conf.js',
        autoWatch: true,
        singleRun: false
      },
      unit_coverage: {
        configFile: 'test/unit/karma.conf.js',
        autoWatch: false,
        singleRun: true,
        reporters: ['progress', 'coverage'],
        preprocessors: {
          'app/js/**/*.js': ['coverage']
        },
        coverageReporter: {
          type : 'html',
          dir : 'coverage/'
        }
      },
    },
    watch: {
      lessmini: {
        files: ['app/styles/styles.less'],
        tasks: ['less','autoprefixer','cssmin']
      },
      jsmini: {
        files: ['app/js/**/*.js'],
        tasks: ['ngmin','concat','uglify']
      },
      ngtemplates: {
        files: ["views/**/**/*.jade", "views/**/*.jade"],
        tasks: ['jade','ngtemplates','concat','uglify']
      }
    }
  });

  // NPM Tasks
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-angular-templates');
  grunt.loadNpmTasks('grunt-jade');
  //grunt.loadNpmTasks('grunt-assets-versioning');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-shell');
  grunt.loadNpmTasks('grunt-protractor-runner');
  grunt.loadNpmTasks('grunt-rev');
  grunt.loadNpmTasks('grunt-forcemin');
  grunt.loadNpmTasks('grunt-ngmin');
  grunt.loadNpmTasks('grunt-shell-spawn');

  // Set Default Tasks
  grunt.registerTask('default', [
    'jade',
    'ngtemplates',
    'less',
    'autoprefixer',
    'cssmin',
    'ngmin',
    'concat',
    'uglify',
    //'rev',
    //'forcemin',
    'watch',
  ]);

  //single run tests
  grunt.registerTask('test:unit', ['karma:unit']);

  //autotest and watch tests
  grunt.registerTask('autotest', ['karma:unit_auto']);

  //coverage testing
  grunt.registerTask('test:coverage', ['karma:unit_coverage']);

};