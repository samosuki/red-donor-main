angular.module('reddonor').run(['$templateCache', function($templateCache) {

  $templateCache.put('views/partials/templates/about-blood.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>About Blood</h1>\n" +
    "        <h2>Blood is the fluid that transports oxygen and nutrients around the body. An average adult has just under 5 litres of blood circulating around their body.</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row\">\n" +
    "    <aboutnav></aboutnav>\n" +
    "  </div>\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h2 class=\"title\"> Blood Types</h2>\n" +
    "      <div class=\"introduction\">\n" +
    "        <h2>Knowing your blood type is important. When there is a critical need for a specific blood type you can be ready to answer the call.</h2>\n" +
    "      </div>\n" +
    "      <!--img.img-responsive(src=\"/assets/images/gallery/blood-types.jpg\").thumbnail.pull-right-->\n" +
    "      <h4>THE MAIN BLOOD GROUPS</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Blood consists of many major group systems, but the most important system in blood donation is the ABO Blood Group System. This system consists of:</p>\n" +
    "        <p>Group O – has neither A nor B antigens on red cells</p>\n" +
    "        <p>Group A – has only the A antigen on red cells</p>\n" +
    "        <p>Group B – has only the B antigen on red cells</p>\n" +
    "        <p>Group AB – has both A and B antigens on red cells</p>\n" +
    "        <p>Each blood group is also classified by either a Rh factor of Rh negative (-) or Rh positive. (+)</p>\n" +
    "      </div>\n" +
    "      <h4>BLOOD RECIPIENTS & DONORS</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Blood Type O- can donate to anyone. It is the Universal Donor.</p>\n" +
    "        <p>Blood Type O+ can donate to O+, A+, B+ and AB+.</p>\n" +
    "        <p>Blood Type A+ can donate to A+ and AB+.</p>\n" +
    "        <p>Blood Type A- can donate to A+, A-, AB+ and AB-.</p>\n" +
    "        <p>Blood Type B+ can donate to B+ and AB+.</p>\n" +
    "        <p>Blood Type B- can donate to B+, B-, AB+ and AB-.</p>\n" +
    "        <p>Blood Type AB+ can donate to AB+, but can receive from anyone. It is the Universal Recipient.</p>\n" +
    "        <p>Blood Type AB- can donate to AB+ and AB-.</p>\n" +
    "      </div>\n" +
    "      <h4>BLOOD GROUP INHERITANCE</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Blood groups are inherited from your parents. The four blood groups, O, A, B and AB are controlled by the A, B and O genes. The A and B genes are dominant and group O is dependent on the inheritance of an O gene from each parent.</p>\n" +
    "        <p>The inheritance of the Rhesus D (Rh D)gene from either or both parents results in the Rh D positive blood group “expression”. The absence of the Rh D gene results in the expression of the Rh D negative blood group.</p>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/aboutblood-nav.html',
    "\n" +
    "<div class=\"col-md-10 col-md-offset-1\">\n" +
    "  <div class=\"submenu hidden-xs\">\n" +
    "    <ul>\n" +
    "      <li><a href=\"/aboutblood/bloodtypes\">Blood Types</a></li><i class=\"fa fa-heart\"></i>\n" +
    "      <li><a href=\"/aboutblood/safety\">Blood Safety</a></li><i class=\"fa fa-heart\"></i>\n" +
    "      <li><a href=\"/aboutblood/uses\">Uses of Blood</a></li><i class=\"fa fa-heart\"></i>\n" +
    "      <li><a href=\"/aboutblood/faqs\">FAQs</a></li>\n" +
    "    </ul>\n" +
    "  </div>\n" +
    "  <div class=\"submenu-small visible-xs\">\n" +
    "    <ul>\n" +
    "      <li><a href=\"/aboutblood/bloodtypes\">Blood Types</a></li>\n" +
    "      <li><a href=\"/aboutblood/safety\">Blood Safety</a></li>\n" +
    "      <li><a href=\"/aboutblood/uses\">Uses of Blood</a></li>\n" +
    "      <li><a href=\"/aboutblood/faqs\">FAQs</a></li>\n" +
    "    </ul>\n" +
    "  </div>\n" +
    "</div>"
  );


  $templateCache.put('views/partials/templates/aboutus.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>About Us</h1>\n" +
    "        <h2>Red Donor is a social enterprise devoted to increasing the capacity of a safe and constant supply of blood for every member of the community.</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h4>Our Story</h4>\n" +
    "      <p>Samora Reid has always been passionate about two things - technology and how information helps us understand how things work. These two passions have joined with his experiences with blood donation to create a movement for a better world.</p>\n" +
    "      <p>In 2006, he went to donate blood for the first time for his late grandfather. Like most people, after the task was done he returned to his regular routine without giving it a second thought. Within the following years, Samora would be exposed to the frightening experience of the importance of blood in saving the life of the father of a friend.</p>\n" +
    "      <p>Months later, he realised many families must go through that same experience, wondering if there is enough blood to save the life of their loved ones. In September of 2012, Samora started building a web application to connect blood donors to those who needed the vital commodity. Inspired by the culture of the Caribbean that idea grew into the founding of Red Donor. An organisation aimed at utilising the close-knit community spirit of the Caribbean infused with innovative technological and creative solutions to encourage more people to donate blood.</p>\n" +
    "      <p>Red Donor's approach to achieving this goal is by observing the behaviours and trends within the community and analysing that data to create more altrustic donor communities within the region.</p><br/>\n" +
    "      <h4>Our Team</h4>\n" +
    "      <div style=\"text-align: center;\" class=\"row\">\n" +
    "        <div class=\"col-md-6\"><img src=\"http://org.reddonor.com/wp-content/themes/Red_Donor_BWP/img/samorareid.png\" alt=\"image\"/>\n" +
    "          <h4>Samora Reid</h4>\n" +
    "          <h5>Founder</h5>\n" +
    "          <p>Samora is a freelance web developer. His experiences include building websites for several industries. Red Donor is his first foray into the world of social entrepreneurship and changing the world through technology.</p>\n" +
    "          <p>In his spare time he can be found listening to music or playing videogames with a cup of coffee nearby.</p>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-6\"> <img src=\"http://org.reddonor.com/wp-content/themes/Red_Donor_BWP/img/joshuaclarke.png\" alt=\"image\"/>\n" +
    "          <h4>Joshua Clarke</h4>\n" +
    "          <h5>Creative Director</h5>\n" +
    "          <p>Joshua Clarke was always found with a pencil in hand doodling on some artwork. In his final year of a Graphic Design Bsc and a regular exhibitor at AnimeKon. Joshua uses non-traditional artistic inspiration to fuel his advertising design strategies.</p>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/blood-faqs.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>About Blood</h1>\n" +
    "        <h2>Blood is the fluid that transports oxygen and nutrients around the body. An average adult has just under 5 litres of blood circulating around their body.</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row\">\n" +
    "    <aboutnav></aboutnav>\n" +
    "  </div>\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h2 class=\"title\">FAQs</h2>\n" +
    "      <div class=\"introduction\">\n" +
    "        <h2>Answers to your questions about blood and the blood donation process.</h2>\n" +
    "      </div>\n" +
    "      <h4>WHAT ARE THE DIFFERENT BLOOD TYPES?</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>There are two major blood type systems, the ABO system and the Rhesus (Rh factor) system. These blood systems created the 8 major blood types – O+, O-, A+, A-, B+, B-, AB+ and AB-.</p>\n" +
    "      </div>\n" +
    "      <h4>WHAT ARE THE DIFFERENT COMPONENTS OF BLOOD AND WHAT DO THEY DO?</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <h4>Red Cells</h4>\n" +
    "        <p>The main function of red blood cells is to distribute oxygen to body tissues and to carry waste carbon dioxide back to the lungs. These are used in the treatment of anaemia, cancer, premature newborns, sickle cell disease and to replace lost blood in accidents, surgery and after childbirth.</p>\n" +
    "        <p>Red cells are stored in a refrigerator and have a shelf life of up to 42 days.</p>\n" +
    "        <h4>Plasma</h4>\n" +
    "        <p>Plasma is the straw coloured fluid in which the red cells, white cells and platelets are suspended. Plasma is the most versatile component of blood as it can be processed into a variety of products and each product can be used to treat a number of potentially life-threatening conditions.</p>\n" +
    "        <p>Plasma is stored frozen and has a shelf life of up to 12 months.</p>\n" +
    "        <h4>Platelets</h4>\n" +
    "        <p>Platelets are components of blood that assist in the blood clotting process.</p>\n" +
    "      </div>\n" +
    "      <h4>HOW MUCH BLOOD IS TAKEN?</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>A single unit of blood taken during a whole blood donation is about 470mL (about a pint). This amount is less than 10% of your total blood volume. Your body keeps on replenishing blood all the time whether you give blood or not, so this amount is quickly replaced.</p>\n" +
    "      </div>\n" +
    "      <h4>HOW MUCH BLOOD DOES AN ADULT BODY HAVE?</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>An average size adult has a blood volume of about 5 litres.</p>\n" +
    "      </div>\n" +
    "      <h4>WHAT HAPPENS TO THE BLOOD ONCE I’VE DONATED?</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Whole blood donations are separated into their components (red cells, platelets and plasma). After processing, red cells are refrigerated and can be stored for up to 42 days. Platelets are stored at room temperature and can be stored for up to 5 days. Plasma is frozen and can be stored for up to 12 months.</p>\n" +
    "        <p>Whilst a significant proportion of the plasma is used for direct transfusion to patients, the majority of donated plasma is further processed into a number of very important plasma products. These plasma products include immunisations against chicken pox, hepatitis B and tetanus; clotting factors for the treatment of patients with haemophilia; protein products for the treatment of patients with burns, liver and kidney diseases; and immunoglobulin products for the treatment of patients with antibody deficiencies and other disorders of the immune system.</p>\n" +
    "      </div>\n" +
    "      <h4>HOW LONG UNTIL MY BLOOD IS USED?</h4>\n" +
    "      <div class=\"blurb\">  \n" +
    "        <p>All blood donations are tested and processed and available for use between 24 and 48 hours after collection. Whole blood is separated into its components (red cells, platelets, plasma). After processing, red cells can be stored for up to 42 days; plasma is frozen and can be stored for up to 12 months; and platelets have a shelf-life of only five days.</p>\n" +
    "      </div>\n" +
    "      <h4>WHAT IS MY BLOOD TESTED FOR?</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>After donation, all blood is tested at every donation for blood type, the presence of red cell antibodies and for the following infections: HIV1 & 2, hepatitis B & C, HTLV I & II and syphilis. Some donations are also tested for malaria depending on the donor’s answers to the questions on the donor questionnaire. </p>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/blood-safety.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>About Blood</h1>\n" +
    "        <h2>Blood is the fluid that transports oxygen and nutrients around the body. An average adult has just under 5 litres of blood circulating around their body.</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row\">\n" +
    "    <aboutnav></aboutnav>\n" +
    "  </div>\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h2 class=\"title\">Blood Safety</h2>\n" +
    "      <div class=\"introduction\">\n" +
    "        <h2>Several guidelines and testing procedures exist to ensure the safety of donors, recipients and the blood supply.</h2>\n" +
    "      </div>\n" +
    "      <h4>DONOR SAFETY</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>The mini physical performed by the Blood Collecting Centre before your blood donation is designed to help them spot potential problems. Sterile equipment and best practices are utilised to reduce any potential risk of infection.</p>\n" +
    "      </div>\n" +
    "      <h4>RECIPIENT SAFETY</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Blood Collecting Centres perform a range of laboratory tests on every unit of blood donated. The tests include:</p>\n" +
    "        <p>ABO Blood Type</p>\n" +
    "        <p>Rh Groups (Positive or Negative)</p>\n" +
    "        <p>HIV/AIDS</p>\n" +
    "        <p>Hepatitis B & C</p>\n" +
    "        <p>Syphilis</p>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/blood-uses.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>About Blood</h1>\n" +
    "        <h2>Blood is the fluid that transports oxygen and nutrients around the body. An average adult has just under 5 litres of blood circulating around their body.</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row\">\n" +
    "    <aboutnav></aboutnav>\n" +
    "  </div>\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h2 class=\"title\">Uses of Blood</h2>\n" +
    "      <div class=\"introduction\">\n" +
    "        <h2>Blood collected is not just used for emergency operations or accident victims. Many patients could not survive without blood transfusions.</h2>\n" +
    "      </div>\n" +
    "      <h4>HOW DONATED BLOOD IS USED</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>The blood collected is separated into its 3 main components:</p>\n" +
    "        <p>Red Cells</p>\n" +
    "        <p>Plasma</p>\n" +
    "        <p>Platelets</p>\n" +
    "      </div>\n" +
    "      <h4>RED CELLS</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>The main function of red blood cells is to distribute oxygen to body tissues and to carry waste carbon dioxide back to the lungs.</p>\n" +
    "        <p>These are used in the treatment of anaemia, cancer, premature newborns, sickle cell disease and to replace lost blood in accidents, surgery and after childbirth.</p>\n" +
    "      </div>\n" +
    "      <h4>PLASMA</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Fresh frozen plasma is used after obstetric loss of blood (which is usually childbirth), during cardiac surgery, and to reverse any anti-coagulant treatment. It is also used to replace clotting factors after massive transfusions or when they are not being sufficiently produced, such as liver disease.</p>\n" +
    "        <p>Processed plasma has several important uses. It is used in the treatment of haemophilia and for treating sufferers of Christmas disease, a life-threatening form of haemophilia. Processed plasma is also used to help produce stronger antibodies against diseases like tetanus, hepatitis, chickenpox and rabies. It also helps generate anti-D, which is used for RhD negative pregnant women carrying RhD positive babies. Additionally there is a protein called albumin contained in plasma, which is extremely beneficial for burn victims.</p>\n" +
    "      </div>\n" +
    "      <h4>PLATELETS</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Platelets can be used in bone marrow failure, post transplant and chemotherapy treatments, and leukaemia. Platelets can be of huge benefit to the recipient.</p>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/contact.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>Contact Us</h1>\n" +
    "        <h2>We're open to any questions, concerns, comments or suggestions you may have. Feel free to reach out to us via email or social media.\t\t\t\t</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <div class=\"blurb\">\n" +
    "        <h4>To Report Problems with the Website</h4>\n" +
    "        <p><a href=\"mailto:support@reddonor.com\">support@reddonor.com</a></p>\n" +
    "        <h4>For Questions Or Suggestions about the Organisation</h4>\n" +
    "        <p><a href=\"mailto:sreid@reddonor.com\">sreid@reddonor.com</a></p>\n" +
    "        <h4>Social</h4>\n" +
    "        <p>Like us on <a href=\"https://www.facebook.com/RedDonor\" target=\"_blank\">Facebook</a></p>\n" +
    "        <p>Follow us on <a href=\"https://twitter.com/RedDonor\" target=\"_blank\">Twitter</a></p>\n" +
    "        <p>Follow us on <a href=\"https://plus.google.com/u/0/b/110993637022991434407/110993637022991434407/posts\" target=\"_blank\">Google+              </a></p>\n" +
    "        <p>Subscribe on <a href=\"http://www.youtube.com/RedDonor\" target=\"_blank\">YouTube</a></p>\n" +
    "        <p>Join us on <a href=\"http://www.linkedin.com/company/red-donor\" target=\"_blank\">Linkedin</a></p>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/eligibility.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>Give Blood</h1>\n" +
    "        <h2>Blood is a vital lifeline to saving and preserving hundreds of lives daily. Less than 2% of the population donates blood.</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row\">\n" +
    "    <givenav></givenav>\n" +
    "  </div>\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h2 class=\"title\">ELIGIBILITY FOR BLOOD DONATION</h2>\n" +
    "      <div class=\"introduction\">\n" +
    "        <h2>Most people can give blood, but to ensure the safety of blood donors and recipients, all blood donors must match the eligibility criteria.</h2>\n" +
    "      </div>\n" +
    "      <h5>NOTICE</h5>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>The eligibility criteria for blood donors differs from country to country. Therefore blood donations centres in your country may have a different set of requirements.</p>\n" +
    "      </div>\n" +
    "      <h4>YOU CAN GIVE BLOOD IF YOU:</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <ul>\n" +
    "          <li>are between the ages of 18 and 65 and in healthy condition</li>\n" +
    "          <li>weigh more than 110 pounds</li>\n" +
    "          <li>have not had a tattoo or body piercing within the past 12 months</li>\n" +
    "          <li>are not pregnant or nursing</li>\n" +
    "          <li>have no history of jaundice after age 11</li>\n" +
    "        </ul><br/>\n" +
    "        <p>Diabetics are eligible if they are controlled by diet or oral medications, no insulin</p>\n" +
    "        <p>Persons with hypertension are eligible as long as their blood pressure is within a reasonable reading at the time of donation</p>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/faqs.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>FAQs</h1>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h4>What is Red Donor?</h4>\n" +
    "      <p>\n" +
    "        Red Donor is a crowd sourcing platform for blood donations. People from around the Caribbean can post requests for blood and pledge to donate \n" +
    "        blood to people who need it in their communities.\n" +
    "      </p>\n" +
    "      <h4>How does it work?</h4>\n" +
    "      <p>\n" +
    "        Blood donations are pledged via Blood Drives. A Blood Drive can be created by or on behalf of the blood recipient by \n" +
    "        any person. Blood donors with a compatible blood type can pledge to make a donation at their nearest blood donation centre.\n" +
    "      </p>\n" +
    "      <h4>How to pledge a blood donation?</h4>\n" +
    "      <p>After signing up for a Red Donor account, you can browse the active Blood Drives for recipients with compatible blood types.</p>\n" +
    "      <h4>How to donate blood?</h4>\n" +
    "      <p>You can donate at your nearest local blood donation clinic. The opening hours of the local blood donor centres are listed with Blood Drive details.</p>\n" +
    "      <p>The process of donating blood encompasses 3 steps - Preparation for Your Donation, The Donation Process and After the Donation.</p>\n" +
    "      <h5>Preparation For Your Donation</h5>\n" +
    "      <ul>\n" +
    "        <li>The day before you donate, drink plenty of liquids</li>\n" +
    "        <li>Eat something at least 3 hours before you donate</li>\n" +
    "      </ul>\n" +
    "      <h5>Donation Process</h5>\n" +
    "      <ul>\n" +
    "        <li>Before donation you will be asked a few questions to determine if you are eligible to donate blood</li>\n" +
    "        <li>Your hemoglobin level, temperature, pulse and blood will be checked before donation</li>\n" +
    "        <li>Sterile equipment will be used to collect the blood for about 10 minutes</li>\n" +
    "        <li>After donating, you will be asked to rest in the chair for a few minutes during which you can request a drink</li>\n" +
    "      </ul>\n" +
    "      <h5>After Donating</h5>\n" +
    "      <ul>\n" +
    "        <li>Continue to drink water throughout the day</li>\n" +
    "        <li>Try not to over exert yourself for the rest of the day</li>\n" +
    "      </ul>\n" +
    "      <h4>How to start a Blood Drive?</h4>\n" +
    "      <p>\n" +
    "        Click Create A Blood Drive. You can choose to create a blood drive as a guest or sign up for an account first. Type the recipient's name, a description of the reason for the Blood Drive, \n" +
    "        blood type required, country located and the deadline date to start a Blood Drive.\n" +
    "      </p>\n" +
    "      <h4>How to find persons to donate to?</h4>\n" +
    "      <p>The quickest way to find someone to donate to is to sign up for a Red Donor account and click Compatible Drives.</p>\n" +
    "      <h4>Who can donate blood?</h4>\n" +
    "      <p>The eligibility for blood donors differs from country to country. Most people are able to donate blood if they: </p>\n" +
    "      <ul>\n" +
    "        <li>are between the ages of 18 and 65 and in healthy condition</li>\n" +
    "        <li>weigh more than 110 pounds</li>\n" +
    "        <li>have not had a tattoo or body piercing within the past 12 months</li>\n" +
    "        <li>are not pregnant or nursing</li>\n" +
    "        <li>have no history of jaundice after age 11</li>\n" +
    "      </ul>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/giveblood-nav.html',
    "\n" +
    "<div class=\"col-md-10 col-md-offset-1\">\n" +
    "  <div class=\"submenu hidden-xs\">\n" +
    "    <ul>\n" +
    "      <li><a href=\"/giveblood/whydonate\">Why Donate</a></li><i class=\"fa fa-heart\"></i>\n" +
    "      <li><a href=\"/giveblood/eligibility\">Eligibility</a></li><i class=\"fa fa-heart\"></i>\n" +
    "      <li><a href=\"/giveblood/procedure\">Donation Procedure</a></li><i class=\"fa fa-heart\"></i>\n" +
    "      <li><a href=\"/giveblood/tips\">Tips</a></li>\n" +
    "    </ul>\n" +
    "  </div>\n" +
    "  <div class=\"submenu-small visible-xs\">\n" +
    "    <ul>\n" +
    "      <li><a href=\"/giveblood/whydonate\">Why Donate</a></li>\n" +
    "      <li><a href=\"/giveblood/eligibility\">Eligibility</a></li>\n" +
    "      <li><a href=\"/giveblood/procedure\">The Procedure</a></li>\n" +
    "      <li><a href=\"/giveblood/tips\">Tips</a></li>\n" +
    "    </ul>\n" +
    "  </div>\n" +
    "</div>"
  );


  $templateCache.put('views/partials/templates/home.html',
    "\n" +
    "<div class=\"hero-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"hero-section\">\n" +
    "      <h1>Give the gift of life!</h1>\n" +
    "      <p>Someone needs your help today. We can save lives, one donation at a time.</p>\n" +
    "      <p><a href=\"/savealife\" class=\"herobtn\">How To Save A Life?</a>\n" +
    "        <!--a.herobtn(href='http://my.reddonor.com') COMING SOON-->\n" +
    "      </p>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>\n" +
    "<section class=\"container introduction\">\n" +
    "  <div class=\"row\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h2>Red Donor is a social enterprise devoted to increasing the capacity of a safe and constant supply of blood for every member of the community.</h2>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "  <div class=\"row\">\n" +
    "    <div class=\"col-md-4\"><img src=\"assets/images/bloodcrisis-md.jpg\" alt=\"Help Save A life\" title=\"&copy; Photo credit: Jason Hargrove - flickr.com/photos/salty_soul/4165610216\" class=\"img-rounded img-responsive hidden-sm hidden-xs\"/><img src=\"assets/images/bloodcrisis-sm.jpg\" alt=\"Help Save A life\" title=\"&copy; Photo credit: Jason Hargrove - flickr.com/photos/salty_soul/4165610216\" class=\"img-rounded img-responsive visible-sm visible-xs\"/>\n" +
    "      <h4><a href=\"/giveblood\">Help Save A Life</a></h4>\n" +
    "      <p>Donate blood and help save the life of someone in your community. 1 donation of blood can save up to 3 lives.</p>\n" +
    "    </div>\n" +
    "    <div class=\"col-md-4\"><img src=\"assets/images/helpotherblood-md.jpg\" alt=\"Start A Blood Drive\" title=\"&copy; Photo credit: thirteenofclubs - flickr.com/photos/thirteenofclubs/4216252051\" class=\"img-rounded img-responsive hidden-sm hidden-xs\"/><img src=\"assets/images/helpotherblood-sm.jpg\" alt=\"Start A Blood Drive\" title=\"&copy; Photo credit: makelessnoise - flickr.com/photos/thirteenofclubs/4216252051\" class=\"img-rounded img-responsive visible-sm visible-xs\"/>\n" +
    "      <h4><a href=\"/howitworks\">How To Start A Blood Drive</a></h4>\n" +
    "      <p>Join to movement to encourage more people, organisations and business to donate for a worthy cause.</p>\n" +
    "    </div>\n" +
    "    <div class=\"col-md-4\"><img src=\"assets/images/youeligible-md.jpg\" alt=\"Learn About Blood\" title=\"&copy; Photo credit: European Parliament - flickr.com/photos/european_parliament/5933370325\" class=\"img-rounded img-responsive hidden-sm hidden-xs\"/><img src=\"assets/images/youeligible-sm.jpg\" alt=\"Learn About Blood\" title=\"&copy; Photo credit: European Parliament - flickr.com/photos/european_parliament/5933370325\" class=\"img-rounded img-responsive visible-sm visible-xs\"/>\n" +
    "      <h4><a href=\"/aboutblood\">Learn about Blood</a></h4>\n" +
    "      <p>1 in 3 people will need blood in their lifetime and less than 2% of the eligible donors in the community donate blood.</p>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h2>Every day hundreds of people are in need of blood. Pledge to donate. You can help save a life.</h2>\n" +
    "        <div class=\"col-sm-6\"><a href=\"/aboutblood\" class=\"regbtn\">Learn About Blood Donation </a></div>\n" +
    "        <div class=\"col-sm-6\"><a href=\"http://my.reddonor.com/create\" class=\"regbtn\">Start a Blood Drive</a></div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/howitworks.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>How It Works</h1>\n" +
    "        <h2>...</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-12\">\n" +
    "      <div class=\"row\">\n" +
    "        <div class=\"col-md-4\">\n" +
    "          <div class=\"works\">\n" +
    "            <h3 class=\"title\">For Blood Requesters</h3>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/join2.png\" alt=\"Join Red Donor\"/>\n" +
    "              <h3>Join Red Donor</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Simple Sign Up </p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/create.png\" alt=\"Create a blood drive\"/>\n" +
    "              <h3>Create a Blood Drive</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Make a request for blood</p>\n" +
    "                <p>Set a donation period </p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/receive.png\" alt=\"Receive pledges to donate\"/>\n" +
    "              <h3>Receive Pledges to Donate</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Willing Blood Donors Pledge to Donate</p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/pledged.png\" alt=\"Receive confirmation\"/>\n" +
    "              <h3>Receive Confirmation</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Blood Donors confirm donation at local collection centre</p>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "          <div class=\"works\">\n" +
    "            <h3 class=\"title\">For Blood Donors</h3>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/join2.png\" alt=\"Join Red Donor\"/>\n" +
    "              <h3>Join Red Donor</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Simple Sign Up </p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/find.png\" alt=\"Find Blood Drives\"/>\n" +
    "              <h3>Find Blood Drives</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Locate local persons uregently in need of blood </p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/pledge.png\" alt=\"Pledge to donate\"/>\n" +
    "              <h3>Pledge to Donate</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Make a pledge to donate blood at your local donation centre</p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/confirm.png\" alt=\"Confirm Donation\"/>\n" +
    "              <h3>Confirm Donation</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Notify the Blood Requester of Your Donation</p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/happy.png\" alt=\"Feel Good\"/>\n" +
    "              <h3>Feel Good for Saving A Life</h3><br/>\n" +
    "              <div class=\"st-text\"></div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "          <div class=\"works\">\n" +
    "            <h3 class=\"title\">For Organisations</h3>\n" +
    "            <div class=\"st-highlight\">\n" +
    "              <h3>Coming Soon</h3><br/>\n" +
    "              <div class=\"st-text\"></div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/howtosave.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>How To Save A Life?</h1>\n" +
    "        <h2>Red Donor takes the stress out of finding willing blood donors for your loved ones and connects blood donors to those who need their help.</h2>\n" +
    "        <!--h2 Through education and innovation we want to ensure a safe and stable supply of blood for your community.-->\n" +
    "        <div class=\"row\">\n" +
    "          <div class=\"col-md-8 col-md-offset-2\">\n" +
    "            <div class=\"video-container\">\n" +
    "              <iframe width=\"560\" height=\"315\" src=\"//www.youtube.com/embed/9oCR1Yo5em8?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;wmode=transparent\" frameborder=\"0\"></iframe>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"row\">\n" +
    "          <div class=\"col-md-10 col-md-offset-1\">\n" +
    "            <h4>Red Donor allows persons to make public requests for blood for their family, friends and loved ones. People who have signed up for an account can pledge to donate blood to the requested blood donation centre. \t\t\t\t</h4>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-12\">\n" +
    "      <h2>Quick Use Instructions </h2>\n" +
    "      <div class=\"row\">\n" +
    "        <div class=\"col-md-4 col-md-offset-2\">\n" +
    "          <div class=\"works\">\n" +
    "            <h3 class=\"title\">For Blood Requesters</h3>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/join2.png\" alt=\"Join Red Donor\"/>\n" +
    "              <h3>Join Red Donor</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Simple Sign Up </p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/create.png\" alt=\"Create a blood drive\"/>\n" +
    "              <h3>Create a Blood Drive</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Make a request for blood</p>\n" +
    "                <p>Set a donation period </p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/receive.png\" alt=\"Receive pledges to donate\"/>\n" +
    "              <h3>Receive Pledges to Donate</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Willing Blood Donors Pledge to Donate</p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/pledged.png\" alt=\"Receive confirmation\"/>\n" +
    "              <h3>Receive Confirmation</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Blood Donors confirm donation at local collection centre</p>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "          <div class=\"works\">\n" +
    "            <h3 class=\"title\">For Blood Donors</h3>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/join2.png\" alt=\"Join Red Donor\"/>\n" +
    "              <h3>Join Red Donor</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Simple Sign Up </p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/find.png\" alt=\"Find Blood Drives\"/>\n" +
    "              <h3>Find Blood Drives</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Locate local persons uregently in need of blood </p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/pledge.png\" alt=\"Pledge to donate\"/>\n" +
    "              <h3>Pledge to Donate</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Make a pledge to donate blood at your local donation centre</p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/confirm.png\" alt=\"Confirm Donation\"/>\n" +
    "              <h3>Confirm Donation</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Notify the Blood Requester of Your Donation</p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/happy.png\" alt=\"Feel Good\"/>\n" +
    "              <h3>Feel Good for Saving A Life</h3><br/>\n" +
    "              <div class=\"st-text\"></div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div><!--.col-md-4\n" +
    "        <div class=\"works\">\n" +
    "          <h3 class=\"title\">For Organisations</h3>\n" +
    "          <div class=\"st-highlight\">\n" +
    "            <h3>Coming Soon</h3><br/>\n" +
    "            <div class=\"st-text\"></div>\n" +
    "          </div>\n" +
    "        </div>-->\n" +
    "      </div>\n" +
    "      <div class=\"row\">\n" +
    "        <div class=\"col-md-8 col-md-offset-2\"><a href=\"http://my.reddonor.com/signup\" class=\"regbtn\">Sign Up For An Account</a></div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/index.html',
    "<!DOCTYPE html>\n" +
    "<html lang=\"en\" data-ng-app=\"reddonor\">\n" +
    "  <head>\n" +
    "    <!--title Red Donor-->\n" +
    "    <title data-ng-bind=\"page.title\" ng-cloak class=\"ng-cloak\">Red Donor</title>\n" +
    "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
    "    <meta name=\"title\" content=\"Red Donor: Connecting Those Who Need Blood To Blood Donors In Your Community\">\n" +
    "    <meta name=\"description\" content=\"Red Donor makes it easier for those who need blood to connect with blood donors in their community.\">\n" +
    "    <meta name=\"keywords\" content=\"Blood Donations, Blood Donors, Caribbean Blood Donation, Caribbean Blood Donation Centres\">\n" +
    "    <meta property=\"og:title\" content=\"Red Donor: Connecting Those Who Need Blood To Blood Donors In Your Community\">\n" +
    "    <meta property=\"og:description\" content=\"Red Donor makes it easier for those who need blood to connect with blood donors in their community.\">\n" +
    "    <meta property=\"og:type\" content=\"website\">\n" +
    "    <meta property=\"og:url\" content=\"http://www.reddonor.com\">\n" +
    "    <meta property=\"og:site_name\" content=\"Red Donor\">\n" +
    "    <!-- Bootstrap -->\n" +
    "    <!--link(href='/vendor/bootstrap/css/bootstrap.min.css', rel='stylesheet', media='screen')-->\n" +
    "    <!-- Included CSS Files-->\n" +
    "    <link href=\"/styles/styles.min.css\" rel=\"stylesheet\" media=\"screen\">\n" +
    "    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries --><!--[if lt IE 9]>\n" +
    "    <script src=\"/vendor/bootstrap/assets/js/html5shiv.js\"></script>\n" +
    "    <script src=\"/vendor/bootstrap/assets/js/respond.min.js\"></script><![endif]--><!--\n" +
    "    <Included>Web Fonts</Included>-->\n" +
    "    <link rel=\"stylesheet\" href=\"http://fonts.googleapis.com/css?family=Open+Sans:400,600\">\n" +
    "    <link rel=\"stylesheet\" href=\"http://fonts.googleapis.com/css?family=Varela+Round\">\n" +
    "    <link rel=\"stylesheet\" href=\"http://fonts.googleapis.com/css?family=Dosis:500\">\n" +
    "    <link rel=\"stylesheet\" href=\"http://fonts.googleapis.com/css?family=Lato\">\n" +
    "    <link rel=\"stylesheet\" href=\"http://fonts.googleapis.com/css?family=Arvo:400,700\">\n" +
    "    <link rel=\"stylesheet\" href=\"//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css\">\n" +
    "    <link rel=\"shortcut icon\" href=\"/assets/images/favicon.ico\">\n" +
    "    <script type=\"text/javascript\">\n" +
    "      var _gaq = _gaq || [];\n" +
    "      _gaq.push(['_setAccount', 'UA-37326435-1']);\n" +
    "      _gaq.push(['_setDomainName', 'reddonor.com']);\n" +
    "      _gaq.push(['_trackPageview']);\n" +
    "      (function() {\n" +
    "        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;\n" +
    "        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';\n" +
    "        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);\n" +
    "      })();\n" +
    "      \n" +
    "    </script>\n" +
    "  </head>\n" +
    "  <body>\n" +
    "    <div id=\"fb-root\"></div>\n" +
    "    <script type=\"text/javascript\">\n" +
    "      (function(d, s, id) {\n" +
    "      var js, fjs = d.getElementsByTagName(s)[0];\n" +
    "      if (d.getElementById(id)) return;\n" +
    "      js = d.createElement(s); js.id = id;\n" +
    "      js.src = \"//connect.facebook.net/en_US/all.js#xfbml=1&appId=222723707782303\";\n" +
    "      fjs.parentNode.insertBefore(js, fjs);\n" +
    "      }(document, 'script', 'facebook-jssdk'));\n" +
    "      \n" +
    "    </script>\n" +
    "    <!--#wrap-->\n" +
    "    <!-- Fixed Navigation Bar ================================================== -->\n" +
    "    <div role=\"navigation\" class=\"navbar navbar-default navbar-fixed-top\">\n" +
    "      <div class=\"container\">\n" +
    "        <div class=\"navbar-header\">\n" +
    "          <button type=\"button\" data-toggle=\"collapse\" data-target=\".navbar-collapse\" class=\"navbar-toggle\"><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span></button><a href=\"/\" class=\"navbar-brand\"><img src=\"/assets/images/red-donor-logo.png\"></a>\n" +
    "        </div>\n" +
    "        <div class=\"collapse navbar-collapse\">\n" +
    "          <ul class=\"nav navbar-nav navbar-right\">\n" +
    "            <!--li: a(href='/donate') Donate-->\n" +
    "            <li><a href=\"/aboutblood\">About Blood</a></li>\n" +
    "            <li><a href=\"/giveblood\">Giving Blood</a></li>\n" +
    "            <li><a href=\"/about\">About Us</a></li>\n" +
    "            <li><a href=\"http://my.reddonor.com\" class=\"menubtn\">Login / Signup</a></li>\n" +
    "          </ul>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "    <!-- Main Content-->\n" +
    "    <div ng-view><!--\n" +
    "      <Footer>(Sticky) ================================================== </Footer>-->\n" +
    "    </div>\n" +
    "    <div id=\"footer\">\n" +
    "      <div class=\"container footer\">\n" +
    "        <div class=\"row\">\n" +
    "          <div class=\"col-sm-3 footer-info\"><a href=\"/\"><img src=\"/assets/images/red-donor-logo.png\"></a>\n" +
    "            <p>Red Donor is focused on increasing the amount of blood available to communities for medical treatment and disaster relief.</p>\n" +
    "          </div>\n" +
    "          <div class=\"col-sm-2 col-xs-4\">\n" +
    "            <h4>About Us</h4>\n" +
    "            <ul>\n" +
    "              <li><a href=\"/about\">Our Story</a></li>\n" +
    "              <li><a href=\"/howitworks\">How It Works</a></li><!--li<a href=\"/partners\">Partners</a>--><!--li<a href=\"/advertise\">Advertising          </a>-->\n" +
    "            </ul>\n" +
    "          </div>\n" +
    "          <div class=\"col-sm-2 col-xs-4\">\n" +
    "            <h4>Get Invovled</h4>\n" +
    "            <ul><!--li<a href=\"/donate\">Donate</a>-->\n" +
    "              <li><a href=\"/faqs\">FAQs</a></li>\n" +
    "              <li><a href=\"/contact\">Contact</a></li>\n" +
    "            </ul>\n" +
    "          </div>\n" +
    "          <div class=\"col-sm-2 col-xs-4\">\n" +
    "            <h4>Connect</h4>\n" +
    "            <ul>\n" +
    "              <li><a href=\"http://blog.reddonor.com\" target=\"_blank\">News</a></li>\n" +
    "              <li><a href=\"https://www.facebook.com/RedDonor\" target=\"_blank\">Facebook</a></li>\n" +
    "              <li><a href=\"https://twitter.com/reddonor\" target=\"_blank\">Twitter</a></li>\n" +
    "            </ul>\n" +
    "          </div>\n" +
    "          <div class=\"col-sm-3 hidden-xs\">\n" +
    "            <div id=\"likebox-frame\">\n" +
    "              <div data-href=\"https://www.facebook.com/RedDonor\" data-width=\"292\" data-colorscheme=\"dark\" data-show-faces=\"true\" data-header=\"false\" data-stream=\"false\" data-show-border=\"false\" class=\"fb-like-box\"></div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "    <div class=\"footer-bottom\">\n" +
    "      <div class=\"container\">\n" +
    "        <div class=\"row\">\n" +
    "          <ul>\n" +
    "            <li><a href=\"/\">© 2013 <span>Red Donor</span></a></li>\n" +
    "            <li><a href=\"/termsofuse\">Terms of Use</a></li>\n" +
    "            <li><a href=\"/privacy\">Privacy Policy  </a></li>\n" +
    "          </ul>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->\n" +
    "    <script src=\"//code.jquery.com/jquery.js\"></script>\n" +
    "    <!-- AngularJs-->\n" +
    "    <script src=\"https://ajax.googleapis.com/ajax/libs/angularjs/1.0.8/angular.min.js\"></script>\n" +
    "    <script src=\"/assets/js/app.min.js\"></script>\n" +
    "    <!-- Include all compiled plugins (below), or include individual files as needed -->\n" +
    "    <!--script(src='vendor/bootstrap/js/bootstrap.min.js')-->\n" +
    "    <!--script(src='vendor/holder/holder.min.js')-->\n" +
    "  </body>\n" +
    "</html>"
  );


  $templateCache.put('views/partials/templates/privacy.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>Privacy Policy</h1>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <p>This Privacy Policy governs the manner in which Red Donor collects, uses, maintains and discloses information collected from users (each, a \"User\") of the www.reddonor.com website (\"Site\"). This privacy policy applies to the Site and all products and services offered by Red Donor.</p>\n" +
    "      <h4>Personal identification information</h4>\n" +
    "      <p>We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the site, subscribe to the newsletter, fill out a form, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, name, email address. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</p>\n" +
    "      <h4>Non-personal identification information</h4>\n" +
    "      <p>We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.</p>\n" +
    "      <h4>Web browser cookies</h4>\n" +
    "      <p>Our Site may use \"cookies\" to enhance User experience. User's web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>\n" +
    "      <h4>How we use collected information</h4>\n" +
    "      <p>Red Donor may collect and use Users personal information for the following purposes:</p>\n" +
    "      <ul>\n" +
    "        <li>To improve customer service\n" +
    "          <p>Information you provide helps us respond to your customer service requests and support needs more efficiently.</p>\n" +
    "        </li>\n" +
    "        <li>To personalize user experience\n" +
    "          <p>We may use information in the aggregate to understand how our Users as a group use the services and resources provided on our Site.</p>\n" +
    "        </li>\n" +
    "        <li>To improve our Site\n" +
    "          <p>We may use feedback you provide to improve our products and services.</p>\n" +
    "        </li>\n" +
    "        <li>To run a promotion, contest, survey or other Site feature\n" +
    "          <p>To send Users information they agreed to receive about topics we think will be of interest to them.</p>\n" +
    "        </li>\n" +
    "        <li>To send periodic emails</li>\n" +
    "      </ul>\n" +
    "      <p>We may use the email address to respond to their inquiries, questions, and/or other requests. If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email or User may contact us via our Site.</p>\n" +
    "      <h4>How we protect your information</h4>\n" +
    "      <p>We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.</p>\n" +
    "      <h4>Sharing your personal information</h4>\n" +
    "      <p>We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above. We may use third party service providers to help us operate our business and the Site or administer activities on our behalf, such as sending out newsletters or surveys. We may share your information with these third parties for those limited purposes provided that you have given us your permission.</p>\n" +
    "      <h4>Compliance with children's online privacy protection act</h4>\n" +
    "      <p>Protecting the privacy of the very young is especially important. For that reason, we never collect or maintain information at our Site from those we actually know are under 13, and no part of our website is structured to attract anyone under 13.</p>\n" +
    "      <h4>Changes to this privacy policy</h4>\n" +
    "      <p>Red Donor has the discretion to update this privacy policy at any time. When we do, we will revise the updated date at the bottom of this page. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>\n" +
    "      <h4>Your acceptance of these terms</h4>\n" +
    "      <p>By using this Site, you signify your acceptance of this policy. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes. </p>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/procedure.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>Give Blood</h1>\n" +
    "        <h2>Blood is a vital lifeline to saving and preserving hundreds of lives daily. Less than 2% of the population donates blood.</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row\">\n" +
    "    <givenav></givenav>\n" +
    "  </div>\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h2 class=\"title\">THE DONATION PROCEDURE</h2>\n" +
    "      <div class=\"introduction\">\n" +
    "        <h2>Giving blood is a simple act of kindness. You can play a vital role in providing blood to save lives.</h2>\n" +
    "      </div>\n" +
    "      <h4>BEFORE DONATING</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>The day before you donate, drink plenty of liquids</p>\n" +
    "        <p>Eat something at least 3 hours before you donate</p>\n" +
    "        <p>Wear clothing with sleeves that can be rolled up above the elbow</p>\n" +
    "        <p>Bring a form of identification</p>\n" +
    "      </div>\n" +
    "      <h4>DURING THE DONATION</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Before donation you will be asked a few questions to determine if you are eligible to donate blood and asked to show your identification.</p>\n" +
    "        <p>Your answers to the interview questions will be private and confidential.</p>\n" +
    "        <p>Your hemoglobin level, temperature, pulse and blood will be checked before donation.</p>\n" +
    "        <p>An area on your arm will be cleaned and a sterile needle will be inserted.</p>\n" +
    "        <p>The blood collection process will last about 10 minutes, until a pint of blood has been collected.</p>\n" +
    "        <p>After donating, you will be asked to rest in the chair for a few minutes during which you can request a drink.</p>\n" +
    "      </div>\n" +
    "      <h4>AFTER DONATING</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Continue to drink water throughout the day.</p>\n" +
    "        <p>Try not to over exert yourself for the rest of the day.</p>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/terms.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>Terms and Conditions of Use</h1>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h4>1. Terms</h4>\n" +
    "      <p>These terms and conditions govern your use of this website; by using this website, you accept these \n" +
    "        <terms>and conditions in full and without reservation. If you disagree with these terms and conditions or </terms>\n" +
    "        <any>part of these terms and conditions, you must not use this website.</any>\n" +
    "      </p>\n" +
    "      <h4>2. Use License</h4>\n" +
    "      <p>Unless otherwise stated, Red Donor owns the intellectual property rights published on this website and materials used on Red Donor. Subject to the license below, all these intellectual property rights are reserved.</p>\n" +
    "      <p>You may view, download for caching purposes only, and print pages, files or other content from the website for your own personal use, \n" +
    "        <subject>to the restrictions set out below and elsewhere in these terms and conditions.</subject>\n" +
    "      </p>\n" +
    "      <p>You must not: </p>\n" +
    "      <ul>\n" +
    "        <li>republish material from this website in neither print nor digital media or documents (including republication on another website);</li>\n" +
    "        <li>sell, rent or sub-license material from the website;</li>\n" +
    "        <li>show any material from the website in public;</li>\n" +
    "        <li>reproduce, duplicate, copy or otherwise exploit material on this website for a commercial purpose;</li>\n" +
    "        <li>edit or otherwise modify any material on the website;</li>\n" +
    "        <li>redistribute material from this website - except for content specifically and expressly made available for redistribution; or</li>\n" +
    "        <li>republish or reproduce any part of this website through the use of iframes or screenscrapers.</li>\n" +
    "      </ul>\n" +
    "      <p>Where content is specifically made available for redistribution, it may only be redistributed within your organisation.</p>\n" +
    "      <h4>3. Acceptable Use</h4>\n" +
    "      <p>\n" +
    "        You must not use this website in any way that causes, or may cause, damage to the website or impairment of the availability \n" +
    "        or accessibility of Red Donor or in any way which is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity.\n" +
    "      </p>\n" +
    "      <p>\n" +
    "        You must not use this website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, \n" +
    "        worm, keystroke logger, rootkit or other malicious computer software.\n" +
    "      </p>\n" +
    "      <p>You must not conduct any systematic or automated data collection activities on or in relation to this website without Red Donor’s express written consent.</p>\n" +
    "      <p>This includes:</p>\n" +
    "      <ul>\n" +
    "        <li>scraping</li>\n" +
    "        <li>data mining</li>\n" +
    "        <li>data extraction</li>\n" +
    "        <li>data harvesting</li>\n" +
    "        <li>'framing' (iframes)</li>\n" +
    "      </ul>\n" +
    "      <p>You must not use this website or any part of it to transmit or send unsolicited commercial communications.</p>\n" +
    "      <p>You must not use this website for any purposes related to marketing without the express written consent of Red Donor.com.</p>\n" +
    "      <h4>4. Restricted Access</h4>\n" +
    "      <p>\n" +
    "        Access to certain areas of this website is restricted. www.reddonor.com reserves the right to restrict access to certain areas of this website, \n" +
    "        or at our discretion, this entire website. www.reddonor.com may change or modify this policy without notice.\n" +
    "      </p>\n" +
    "      <p>\n" +
    "        If www.reddonor.com provides you with a user ID and password to enable you to access restricted areas of this website or other content or services, you must ensure that the \n" +
    "        user ID and password are kept confidential. You alone are responsible for your password and user ID security.\n" +
    "      </p>\n" +
    "      <p>Red Donor.com may disable your user ID and password at www.reddonor.com's sole discretion without notice or explanation.</p>\n" +
    "      <h4>5. User Content</h4>\n" +
    "      <p>In these terms and conditions, “your user content” means material (including without limitation text, images, audio material, video material and audio-visual material) that you submit to this website, for whatever purpose.</p>\n" +
    "      <p>You grant to Red Donor a worldwide, irrevocable, non-exclusive, royalty-free license to use, reproduce, adapt, publish, translate and distribute your user content in any existing or future media. You also grant to Red Donor the right to sub-license these rights, and the right to bring an action for infringement of these rights.</p>\n" +
    "      <p>Your user content must not be illegal or unlawful, must not infringe any third party's legal rights, and must not be capable of giving rise to legal action whether against you or Red Donor or a third party (in each case under any applicable law).</p>\n" +
    "      <p>You must not submit any user content to the website that is or has ever been the subject of any threatened or actual legal proceedings or other similar complaint.</p>\n" +
    "      <p>Red Donor reserves the right to edit or remove any material submitted to this website, or stored on the servers of Red Donor, or hosted or published upon this website.</p>\n" +
    "      <p>Red Donor's rights under these terms and conditions in relation to user content, Red Donor does not undertake to monitor the submission of such content to, or the publication of such content on, this website.</p>\n" +
    "      <h4>6. No Warranties</h4>\n" +
    "      <p>This website is provided “as is” without any representations or warranties, express or implied. www.reddonor.com makes no representations or warranties in relation to this website or the information and materials provided on this website.</p>\n" +
    "      <p>Without prejudice to the generality of the foregoing paragraph, Red Donor does not warrant that:</p>\n" +
    "      <ul> \n" +
    "        <li>this website will be constantly available, or available at all; or</li>\n" +
    "        <li>the information on this website is complete, true, accurate or non-misleading.</li>\n" +
    "      </ul>\n" +
    "      <p>Nothing on this website constitutes, or is meant to constitute, advice of any kind. If you require advice in relation to any legal, financial or medical matter you should consult an appropriate professional.</p>\n" +
    "      <h4>7. Limitations of Liability</h4>\n" +
    "      <p>Red Donor will not be liable to you (whether under the law of contact, the law of torts or otherwise) in relation to the contents of, or use of, or otherwise in connection with, this website:</p>\n" +
    "      <ul> \n" +
    "        <li>to the extent that the website is provided free-of-charge, for any direct loss;</li>\n" +
    "        <li>for any indirect, special or consequential loss; or</li>\n" +
    "        <li>for any business losses, loss of revenue, income, profits or anticipated savings, loss of contracts or business relationships, loss of reputation or goodwill, or loss or corruption of information or data.</li>\n" +
    "      </ul>\n" +
    "      <p>These limitations of liability apply even if Red Donor has been expressly advised of the potential loss.</p>\n" +
    "      <h4>8. Indemnity</h4>\n" +
    "      <p>You hereby indemnify Red Donor and undertake to keep Red Donor indemnified against any losses, damages, costs, liabilities and expenses (including without limitation legal expenses and any amounts paid by www.reddonor.com to a third party in settlement of a claim or dispute on the advice of Red Donor‘s legal advisers) incurred or suffered by Red Donor arising out of any breach by you of any provision of these terms and conditions, or arising out of any claim that you have breached any provision of these terms and conditions.</p>\n" +
    "      <h4>9.\tBreaches of these terms and conditions</h4>\n" +
    "      <p>Without prejudice to Red Donor's other rights under these terms and conditions, if you breach these terms and conditions in any way, www.reddonor.com may take such action as www.reddonor.com deems appropriate to deal with the breach, including suspending your access to the website, prohibiting you from accessing the website, blocking computers using your IP address from accessing the website, contacting your internet service provider to request that they block your access to the website and/or bringing court proceedings against you.</p>\n" +
    "      <h4>10.\tVariation</h4>\n" +
    "      <p>Red Donor may revise these terms and conditions from time-to-time. Revised terms and conditions will apply to the use of this website from the date of the publication of the revised terms and conditions on this website. Please check this page regularly to ensure you are familiar with the current version.</p>\n" +
    "      <h4>11.\tEntire agreement</h4>\n" +
    "      <p>These terms and conditions, together with Red Donor‘s Privacy Policy constitute the entire agreement between you and Red Donor in relation to your use of this website, and supersede all previous agreements in respect of your use of this website.</p>\n" +
    "      <p>This document was last updated on December 08, 2012</p>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/tips.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>Give Blood</h1>\n" +
    "        <h2>Blood is a vital lifeline to saving and preserving hundreds of lives daily. Less than 2% of the population donates blood.</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row\">\n" +
    "    <givenav></givenav>\n" +
    "  </div>\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h2 class=\"title\">DONATION TIPS</h2>\n" +
    "      <div class=\"introduction\">\n" +
    "        <h2>For a safe and successful blood donation, follow these preparations, precautions and recommended tips.</h2>\n" +
    "      </div>\n" +
    "      <h4>BEFORE YOUR DONATION</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Get a good night’s rest. </p>\n" +
    "        <p>Drink plenty of water or non-alcoholic beverages before the donation. Drinking water or fruit juices helps keep your blood pressure up.</p>\n" +
    "        <p>Eat a healthy meal 3 hours before your donation. Eating will keep your blood sugar levels stable and ward off possible dizziness after donation.</p>\n" +
    "        <p>Avoid eating fatty foods before your donation. An increase of fat in your blood stream might make testing of your blood difficult.</p>\n" +
    "        <p>Have your identification card with you at the blood donation centre.</p>\n" +
    "      </div>\n" +
    "      <h4>DURING YOUR DONATION</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Wear clothing with sleeves that can be raised above the elbow.</p>\n" +
    "        <p>Relax, listen to music, read or talk to other donors. Nervousness can cause your blood pressure to drop and can lead to dizziness.</p>\n" +
    "        <p>Have a juice or snack after the donation. Rest for a few minutes to let your body adjust before continuing your day.</p>\n" +
    "      </div>\n" +
    "      <h4>AFTER YOUR DONATION</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Drink plenty of liquids and avoid alcohol over the next 24 hours.</p>\n" +
    "        <p>Eat a high protein meal after your donation.</p>\n" +
    "        <p>Avoid heavy lifting or vigorous exercise for the rest of the day.</p>\n" +
    "        <p>If you experience dizziness after donation, stop what you’re doing and rest or lie down until you feel better.</p>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/why-donate.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>Give Blood</h1>\n" +
    "        <h2>Blood is a vital lifeline to saving and preserving hundreds of lives daily. Less than 2% of the population donates blood.</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row\">\n" +
    "    <givenav></givenav>\n" +
    "  </div>\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h2 class=\"title\">WHY DONATE</h2>\n" +
    "      <div class=\"introduction\">\n" +
    "        <h2>One donation of blood can help to save up to 3 different people.</h2>\n" +
    "      </div>\n" +
    "      <h4>HELPING PEOPLE</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>The blood collected from donations is used for many purposes including:</p>\n" +
    "        <ul>\n" +
    "          <li>Accident Victims</li>\n" +
    "          <li>Emergency Operations</li>\n" +
    "          <li>Organ Transplants</li>\n" +
    "          <li>Leukemia and Cancer Patients</li>\n" +
    "          <li>Cardiac (Heart) Surgeries</li>\n" +
    "          <li>Premature Babies</li>\n" +
    "        </ul>\n" +
    "        <p>Without blood transfusions these patients will not survive. These patients may be a family member, friend or work colleague who may need blood today or one day in the future. Because blood has a limited shelf life, constant donations are needed year round by healthy members of the community.</p>\n" +
    "      </div>\n" +
    "      <h4>HEALTH BENEFITS</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Donating blood also has some health benefits. Before each donation you receive a mini physical to check your:</p>\n" +
    "        <ul>\n" +
    "          <li>Pulse</li>\n" +
    "          <li>Blood Pressure</li>\n" +
    "          <li>Body Temperature</li>\n" +
    "          <li>Haemoglobin</li>\n" +
    "        </ul>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );

}]);
