###
Router for All Pages ***
###

exports.index = (req, res) ->
  console.log "--- Loading Index ---"
  res.render "index"

exports.partials = (req, res) ->
  name = req.params.name
  console.log "--- Loading Page >> " + name
  res.render "partials/" + name

