'use strict';

/* Menu Directives */

reddonor.directive('aboutnav', function() {
		return {
			restrict: 'E',
			replace: true,
			templateUrl: 'views/partials/templates/aboutblood-nav.html'
		};
	})
	.directive('givenav', function() {
			return {
				restrict: 'E',
				replace: true,
				templateUrl: 'views/partials/templates/giveblood-nav.html'
			};
		})