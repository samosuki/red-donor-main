var reddonor = angular.module('reddonor', []);

reddonor.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
          $routeProvider
            .when('/', { title: 'You Can Save A Life', templateUrl: 'views/partials/templates/home.html' })
            .when('/savealife', { title: 'How to Save A Life?', templateUrl: 'views/partials/templates/howtosave.html' })
            .when('/howitworks', { title: 'How It Works', templateUrl: 'views/partials/templates/howtosave.html' })
            .when('/aboutblood', { title: 'About Blood', templateUrl: 'views/partials/templates/about-blood.html', controller: 'AboutBloodCtrl' })
            .when('/aboutblood/bloodtypes', { title: 'Blood Types', templateUrl: 'views/partials/templates/about-blood.html' })
            .when('/aboutblood/safety', { title: 'Blood Safety', templateUrl: 'views/partials/templates/blood-safety.html' })
            .when('/aboutblood/uses', { title: 'Uses of Blood', templateUrl: 'views/partials/templates/blood-uses.html' })
            .when('/aboutblood/faqs', { title: 'FAQs', templateUrl: 'views/partials/templates/blood-faqs.html'})
            .when('/giveblood', { title: 'Give Blood', templateUrl: 'views/partials/templates/why-donate.html'})
            .when('/giveblood/whydonate', { title: 'Why Donate', templateUrl: 'views/partials/templates/why-donate.html'})
            .when('/giveblood/eligibility', { title: 'Eligibility', templateUrl: 'views/partials/templates/eligibility.html'})
            .when('/giveblood/procedure', { title: 'The Donation Procedure', templateUrl: 'views/partials/templates/procedure.html'})
            .when('/giveblood/tips', { title: 'Donation Tips', templateUrl: 'views/partials/templates/tips.html'})
            .when('/termsofuse', { title: 'Terms of Use', templateUrl: 'views/partials/templates/terms.html'})
            .when('/privacy', { title: 'Privacy Policy', templateUrl: 'views/partials/templates/privacy.html'})
            .when('/faqs', { title: 'FAQs', templateUrl: 'views/partials/templates/faqs.html'})
            .when('/contact', { title: 'Contact', templateUrl: 'views/partials/templates/contact.html'})
            .when('/about', { title: 'About Us', templateUrl: 'views/partials/templates/aboutus.html'})
            //.when('/donate', { title: 'How It Works', templateUrl: 'views/partials/templates/howitworks.html'})
            .otherwise({redirectTo:'/'});
          $locationProvider.html5Mode(true);
}]);

reddonor.run(['$rootScope', function($rootScope) {
  $rootScope.page = {
    setTitle: function(title) {
    	this.title = title + ' | Red Donor';
  	}
	}
  $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
  	$rootScope.page.setTitle(current.$$route.title || 'Default Title');
  });
}]);