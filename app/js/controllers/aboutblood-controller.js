/* About Blood Controller */

reddonor.controller('AboutBloodCtrl', ['$scope', '$http', function($scope, $http) {
		// switch between content on About Blood page

		$scope.step = 0;

		$scope.pages = ['bloodtypes','bloodsafety','usesofblood','faqs'];
		$scope.selection = $scope.pages[0];


		$scope.setCurrentPage = function(page) {
			$scope.step = page;
		}

		$scope.getCurrentPage = function() {
			return $scope.pages[$scope.step];
		};
	}]);