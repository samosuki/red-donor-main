var reddonor = angular.module('reddonor', []);

reddonor.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
          $routeProvider
            .when('/', { title: 'You Can Save A Life', templateUrl: 'views/partials/templates/home.html' })
            .when('/savealife', { title: 'How to Save A Life?', templateUrl: 'views/partials/templates/howtosave.html' })
            .when('/howitworks', { title: 'How It Works', templateUrl: 'views/partials/templates/howtosave.html' })
            .when('/aboutblood', { title: 'About Blood', templateUrl: 'views/partials/templates/about-blood.html', controller: 'AboutBloodCtrl' })
            .when('/aboutblood/bloodtypes', { title: 'Blood Types', templateUrl: 'views/partials/templates/about-blood.html' })
            .when('/aboutblood/safety', { title: 'Blood Safety', templateUrl: 'views/partials/templates/blood-safety.html' })
            .when('/aboutblood/uses', { title: 'Uses of Blood', templateUrl: 'views/partials/templates/blood-uses.html' })
            .when('/aboutblood/faqs', { title: 'FAQs', templateUrl: 'views/partials/templates/blood-faqs.html'})
            .when('/giveblood', { title: 'Give Blood', templateUrl: 'views/partials/templates/why-donate.html'})
            .when('/giveblood/whydonate', { title: 'Why Donate', templateUrl: 'views/partials/templates/why-donate.html'})
            .when('/giveblood/eligibility', { title: 'Eligibility', templateUrl: 'views/partials/templates/eligibility.html'})
            .when('/giveblood/procedure', { title: 'The Donation Procedure', templateUrl: 'views/partials/templates/procedure.html'})
            .when('/giveblood/tips', { title: 'Donation Tips', templateUrl: 'views/partials/templates/tips.html'})
            .when('/termsofuse', { title: 'Terms of Use', templateUrl: 'views/partials/templates/terms.html'})
            .when('/privacy', { title: 'Privacy Policy', templateUrl: 'views/partials/templates/privacy.html'})
            .when('/faqs', { title: 'FAQs', templateUrl: 'views/partials/templates/faqs.html'})
            .when('/contact', { title: 'Contact', templateUrl: 'views/partials/templates/contact.html'})
            .when('/about', { title: 'About Us', templateUrl: 'views/partials/templates/aboutus.html'})
            //.when('/donate', { title: 'How It Works', templateUrl: 'views/partials/templates/howitworks.html'})
            .otherwise({redirectTo:'/'});
          $locationProvider.html5Mode(true);
}]);

reddonor.run(['$rootScope', function($rootScope) {
  $rootScope.page = {
    setTitle: function(title) {
    	this.title = title + ' | Red Donor';
  	}
	}
  $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
  	$rootScope.page.setTitle(current.$$route.title || 'Default Title');
  });
}]);;reddonor.controller('AboutBloodCtrl', [
  '$scope',
  '$http',
  function ($scope, $http) {
    $scope.step = 0;
    $scope.pages = [
      'bloodtypes',
      'bloodsafety',
      'usesofblood',
      'faqs'
    ];
    $scope.selection = $scope.pages[0];
    $scope.setCurrentPage = function (page) {
      $scope.step = page;
    };
    $scope.getCurrentPage = function () {
      return $scope.pages[$scope.step];
    };
  }
]);;/* About Blood Controller */

reddonor.controller('AboutBloodCtrl', ['$scope', '$http', function($scope, $http) {
		// switch between content on About Blood page

		$scope.step = 0;

		$scope.pages = ['bloodtypes','bloodsafety','usesofblood','faqs'];
		$scope.selection = $scope.pages[0];


		$scope.setCurrentPage = function(page) {
			$scope.step = page;
		}

		$scope.getCurrentPage = function() {
			return $scope.pages[$scope.step];
		};
	}]);;'use strict';
reddonor.directive('aboutnav', function () {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: 'views/partials/templates/aboutblood-nav.html'
  };
}).directive('givenav', function () {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: 'views/partials/templates/giveblood-nav.html'
  };
});;'use strict';

/* Menu Directives */

reddonor.directive('aboutnav', function() {
		return {
			restrict: 'E',
			replace: true,
			templateUrl: 'views/partials/templates/aboutblood-nav.html'
		};
	})
	.directive('givenav', function() {
			return {
				restrict: 'E',
				replace: true,
				templateUrl: 'views/partials/templates/giveblood-nav.html'
			};
		});/**
* bootstrap.js v3.0.0 by @fat and @mdo
* Copyright 2013 Twitter Inc.
* http://www.apache.org/licenses/LICENSE-2.0
*/
if(!jQuery)throw new Error("Bootstrap requires jQuery");+function(a){"use strict";function b(){var a=document.createElement("bootstrap"),b={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"};for(var c in b)if(void 0!==a.style[c])return{end:b[c]}}a.fn.emulateTransitionEnd=function(b){var c=!1,d=this;a(this).one(a.support.transition.end,function(){c=!0});var e=function(){c||a(d).trigger(a.support.transition.end)};return setTimeout(e,b),this},a(function(){a.support.transition=b()})}(window.jQuery),+function(a){"use strict";var b='[data-dismiss="alert"]',c=function(c){a(c).on("click",b,this.close)};c.prototype.close=function(b){function c(){f.trigger("closed.bs.alert").remove()}var d=a(this),e=d.attr("data-target");e||(e=d.attr("href"),e=e&&e.replace(/.*(?=#[^\s]*$)/,""));var f=a(e);b&&b.preventDefault(),f.length||(f=d.hasClass("alert")?d:d.parent()),f.trigger(b=a.Event("close.bs.alert")),b.isDefaultPrevented()||(f.removeClass("in"),a.support.transition&&f.hasClass("fade")?f.one(a.support.transition.end,c).emulateTransitionEnd(150):c())};var d=a.fn.alert;a.fn.alert=function(b){return this.each(function(){var d=a(this),e=d.data("bs.alert");e||d.data("bs.alert",e=new c(this)),"string"==typeof b&&e[b].call(d)})},a.fn.alert.Constructor=c,a.fn.alert.noConflict=function(){return a.fn.alert=d,this},a(document).on("click.bs.alert.data-api",b,c.prototype.close)}(window.jQuery),+function(a){"use strict";var b=function(c,d){this.$element=a(c),this.options=a.extend({},b.DEFAULTS,d)};b.DEFAULTS={loadingText:"loading..."},b.prototype.setState=function(a){var b="disabled",c=this.$element,d=c.is("input")?"val":"html",e=c.data();a+="Text",e.resetText||c.data("resetText",c[d]()),c[d](e[a]||this.options[a]),setTimeout(function(){"loadingText"==a?c.addClass(b).attr(b,b):c.removeClass(b).removeAttr(b)},0)},b.prototype.toggle=function(){var a=this.$element.closest('[data-toggle="buttons"]');if(a.length){var b=this.$element.find("input").prop("checked",!this.$element.hasClass("active")).trigger("change");"radio"===b.prop("type")&&a.find(".active").removeClass("active")}this.$element.toggleClass("active")};var c=a.fn.button;a.fn.button=function(c){return this.each(function(){var d=a(this),e=d.data("bs.button"),f="object"==typeof c&&c;e||d.data("bs.button",e=new b(this,f)),"toggle"==c?e.toggle():c&&e.setState(c)})},a.fn.button.Constructor=b,a.fn.button.noConflict=function(){return a.fn.button=c,this},a(document).on("click.bs.button.data-api","[data-toggle^=button]",function(b){var c=a(b.target);c.hasClass("btn")||(c=c.closest(".btn")),c.button("toggle"),b.preventDefault()})}(window.jQuery),+function(a){"use strict";var b=function(b,c){this.$element=a(b),this.$indicators=this.$element.find(".carousel-indicators"),this.options=c,this.paused=this.sliding=this.interval=this.$active=this.$items=null,"hover"==this.options.pause&&this.$element.on("mouseenter",a.proxy(this.pause,this)).on("mouseleave",a.proxy(this.cycle,this))};b.DEFAULTS={interval:5e3,pause:"hover",wrap:!0},b.prototype.cycle=function(b){return b||(this.paused=!1),this.interval&&clearInterval(this.interval),this.options.interval&&!this.paused&&(this.interval=setInterval(a.proxy(this.next,this),this.options.interval)),this},b.prototype.getActiveIndex=function(){return this.$active=this.$element.find(".item.active"),this.$items=this.$active.parent().children(),this.$items.index(this.$active)},b.prototype.to=function(b){var c=this,d=this.getActiveIndex();return b>this.$items.length-1||0>b?void 0:this.sliding?this.$element.one("slid",function(){c.to(b)}):d==b?this.pause().cycle():this.slide(b>d?"next":"prev",a(this.$items[b]))},b.prototype.pause=function(b){return b||(this.paused=!0),this.$element.find(".next, .prev").length&&a.support.transition.end&&(this.$element.trigger(a.support.transition.end),this.cycle(!0)),this.interval=clearInterval(this.interval),this},b.prototype.next=function(){return this.sliding?void 0:this.slide("next")},b.prototype.prev=function(){return this.sliding?void 0:this.slide("prev")},b.prototype.slide=function(b,c){var d=this.$element.find(".item.active"),e=c||d[b](),f=this.interval,g="next"==b?"left":"right",h="next"==b?"first":"last",i=this;if(!e.length){if(!this.options.wrap)return;e=this.$element.find(".item")[h]()}this.sliding=!0,f&&this.pause();var j=a.Event("slide.bs.carousel",{relatedTarget:e[0],direction:g});if(!e.hasClass("active")){if(this.$indicators.length&&(this.$indicators.find(".active").removeClass("active"),this.$element.one("slid",function(){var b=a(i.$indicators.children()[i.getActiveIndex()]);b&&b.addClass("active")})),a.support.transition&&this.$element.hasClass("slide")){if(this.$element.trigger(j),j.isDefaultPrevented())return;e.addClass(b),e[0].offsetWidth,d.addClass(g),e.addClass(g),d.one(a.support.transition.end,function(){e.removeClass([b,g].join(" ")).addClass("active"),d.removeClass(["active",g].join(" ")),i.sliding=!1,setTimeout(function(){i.$element.trigger("slid")},0)}).emulateTransitionEnd(600)}else{if(this.$element.trigger(j),j.isDefaultPrevented())return;d.removeClass("active"),e.addClass("active"),this.sliding=!1,this.$element.trigger("slid")}return f&&this.cycle(),this}};var c=a.fn.carousel;a.fn.carousel=function(c){return this.each(function(){var d=a(this),e=d.data("bs.carousel"),f=a.extend({},b.DEFAULTS,d.data(),"object"==typeof c&&c),g="string"==typeof c?c:f.slide;e||d.data("bs.carousel",e=new b(this,f)),"number"==typeof c?e.to(c):g?e[g]():f.interval&&e.pause().cycle()})},a.fn.carousel.Constructor=b,a.fn.carousel.noConflict=function(){return a.fn.carousel=c,this},a(document).on("click.bs.carousel.data-api","[data-slide], [data-slide-to]",function(b){var c,d=a(this),e=a(d.attr("data-target")||(c=d.attr("href"))&&c.replace(/.*(?=#[^\s]+$)/,"")),f=a.extend({},e.data(),d.data()),g=d.attr("data-slide-to");g&&(f.interval=!1),e.carousel(f),(g=d.attr("data-slide-to"))&&e.data("bs.carousel").to(g),b.preventDefault()}),a(window).on("load",function(){a('[data-ride="carousel"]').each(function(){var b=a(this);b.carousel(b.data())})})}(window.jQuery),+function(a){"use strict";var b=function(c,d){this.$element=a(c),this.options=a.extend({},b.DEFAULTS,d),this.transitioning=null,this.options.parent&&(this.$parent=a(this.options.parent)),this.options.toggle&&this.toggle()};b.DEFAULTS={toggle:!0},b.prototype.dimension=function(){var a=this.$element.hasClass("width");return a?"width":"height"},b.prototype.show=function(){if(!this.transitioning&&!this.$element.hasClass("in")){var b=a.Event("show.bs.collapse");if(this.$element.trigger(b),!b.isDefaultPrevented()){var c=this.$parent&&this.$parent.find("> .panel > .in");if(c&&c.length){var d=c.data("bs.collapse");if(d&&d.transitioning)return;c.collapse("hide"),d||c.data("bs.collapse",null)}var e=this.dimension();this.$element.removeClass("collapse").addClass("collapsing")[e](0),this.transitioning=1;var f=function(){this.$element.removeClass("collapsing").addClass("in")[e]("auto"),this.transitioning=0,this.$element.trigger("shown.bs.collapse")};if(!a.support.transition)return f.call(this);var g=a.camelCase(["scroll",e].join("-"));this.$element.one(a.support.transition.end,a.proxy(f,this)).emulateTransitionEnd(350)[e](this.$element[0][g])}}},b.prototype.hide=function(){if(!this.transitioning&&this.$element.hasClass("in")){var b=a.Event("hide.bs.collapse");if(this.$element.trigger(b),!b.isDefaultPrevented()){var c=this.dimension();this.$element[c](this.$element[c]())[0].offsetHeight,this.$element.addClass("collapsing").removeClass("collapse").removeClass("in"),this.transitioning=1;var d=function(){this.transitioning=0,this.$element.trigger("hidden.bs.collapse").removeClass("collapsing").addClass("collapse")};return a.support.transition?(this.$element[c](0).one(a.support.transition.end,a.proxy(d,this)).emulateTransitionEnd(350),void 0):d.call(this)}}},b.prototype.toggle=function(){this[this.$element.hasClass("in")?"hide":"show"]()};var c=a.fn.collapse;a.fn.collapse=function(c){return this.each(function(){var d=a(this),e=d.data("bs.collapse"),f=a.extend({},b.DEFAULTS,d.data(),"object"==typeof c&&c);e||d.data("bs.collapse",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.collapse.Constructor=b,a.fn.collapse.noConflict=function(){return a.fn.collapse=c,this},a(document).on("click.bs.collapse.data-api","[data-toggle=collapse]",function(b){var c,d=a(this),e=d.attr("data-target")||b.preventDefault()||(c=d.attr("href"))&&c.replace(/.*(?=#[^\s]+$)/,""),f=a(e),g=f.data("bs.collapse"),h=g?"toggle":d.data(),i=d.attr("data-parent"),j=i&&a(i);g&&g.transitioning||(j&&j.find('[data-toggle=collapse][data-parent="'+i+'"]').not(d).addClass("collapsed"),d[f.hasClass("in")?"addClass":"removeClass"]("collapsed")),f.collapse(h)})}(window.jQuery),+function(a){"use strict";function b(){a(d).remove(),a(e).each(function(b){var d=c(a(this));d.hasClass("open")&&(d.trigger(b=a.Event("hide.bs.dropdown")),b.isDefaultPrevented()||d.removeClass("open").trigger("hidden.bs.dropdown"))})}function c(b){var c=b.attr("data-target");c||(c=b.attr("href"),c=c&&/#/.test(c)&&c.replace(/.*(?=#[^\s]*$)/,""));var d=c&&a(c);return d&&d.length?d:b.parent()}var d=".dropdown-backdrop",e="[data-toggle=dropdown]",f=function(b){a(b).on("click.bs.dropdown",this.toggle)};f.prototype.toggle=function(d){var e=a(this);if(!e.is(".disabled, :disabled")){var f=c(e),g=f.hasClass("open");if(b(),!g){if("ontouchstart"in document.documentElement&&!f.closest(".navbar-nav").length&&a('<div class="dropdown-backdrop"/>').insertAfter(a(this)).on("click",b),f.trigger(d=a.Event("show.bs.dropdown")),d.isDefaultPrevented())return;f.toggleClass("open").trigger("shown.bs.dropdown"),e.focus()}return!1}},f.prototype.keydown=function(b){if(/(38|40|27)/.test(b.keyCode)){var d=a(this);if(b.preventDefault(),b.stopPropagation(),!d.is(".disabled, :disabled")){var f=c(d),g=f.hasClass("open");if(!g||g&&27==b.keyCode)return 27==b.which&&f.find(e).focus(),d.click();var h=a("[role=menu] li:not(.divider):visible a",f);if(h.length){var i=h.index(h.filter(":focus"));38==b.keyCode&&i>0&&i--,40==b.keyCode&&i<h.length-1&&i++,~i||(i=0),h.eq(i).focus()}}}};var g=a.fn.dropdown;a.fn.dropdown=function(b){return this.each(function(){var c=a(this),d=c.data("dropdown");d||c.data("dropdown",d=new f(this)),"string"==typeof b&&d[b].call(c)})},a.fn.dropdown.Constructor=f,a.fn.dropdown.noConflict=function(){return a.fn.dropdown=g,this},a(document).on("click.bs.dropdown.data-api",b).on("click.bs.dropdown.data-api",".dropdown form",function(a){a.stopPropagation()}).on("click.bs.dropdown.data-api",e,f.prototype.toggle).on("keydown.bs.dropdown.data-api",e+", [role=menu]",f.prototype.keydown)}(window.jQuery),+function(a){"use strict";var b=function(b,c){this.options=c,this.$element=a(b),this.$backdrop=this.isShown=null,this.options.remote&&this.$element.load(this.options.remote)};b.DEFAULTS={backdrop:!0,keyboard:!0,show:!0},b.prototype.toggle=function(a){return this[this.isShown?"hide":"show"](a)},b.prototype.show=function(b){var c=this,d=a.Event("show.bs.modal",{relatedTarget:b});this.$element.trigger(d),this.isShown||d.isDefaultPrevented()||(this.isShown=!0,this.escape(),this.$element.on("click.dismiss.modal",'[data-dismiss="modal"]',a.proxy(this.hide,this)),this.backdrop(function(){var d=a.support.transition&&c.$element.hasClass("fade");c.$element.parent().length||c.$element.appendTo(document.body),c.$element.show(),d&&c.$element[0].offsetWidth,c.$element.addClass("in").attr("aria-hidden",!1),c.enforceFocus();var e=a.Event("shown.bs.modal",{relatedTarget:b});d?c.$element.find(".modal-dialog").one(a.support.transition.end,function(){c.$element.focus().trigger(e)}).emulateTransitionEnd(300):c.$element.focus().trigger(e)}))},b.prototype.hide=function(b){b&&b.preventDefault(),b=a.Event("hide.bs.modal"),this.$element.trigger(b),this.isShown&&!b.isDefaultPrevented()&&(this.isShown=!1,this.escape(),a(document).off("focusin.bs.modal"),this.$element.removeClass("in").attr("aria-hidden",!0).off("click.dismiss.modal"),a.support.transition&&this.$element.hasClass("fade")?this.$element.one(a.support.transition.end,a.proxy(this.hideModal,this)).emulateTransitionEnd(300):this.hideModal())},b.prototype.enforceFocus=function(){a(document).off("focusin.bs.modal").on("focusin.bs.modal",a.proxy(function(a){this.$element[0]===a.target||this.$element.has(a.target).length||this.$element.focus()},this))},b.prototype.escape=function(){this.isShown&&this.options.keyboard?this.$element.on("keyup.dismiss.bs.modal",a.proxy(function(a){27==a.which&&this.hide()},this)):this.isShown||this.$element.off("keyup.dismiss.bs.modal")},b.prototype.hideModal=function(){var a=this;this.$element.hide(),this.backdrop(function(){a.removeBackdrop(),a.$element.trigger("hidden.bs.modal")})},b.prototype.removeBackdrop=function(){this.$backdrop&&this.$backdrop.remove(),this.$backdrop=null},b.prototype.backdrop=function(b){var c=this.$element.hasClass("fade")?"fade":"";if(this.isShown&&this.options.backdrop){var d=a.support.transition&&c;if(this.$backdrop=a('<div class="modal-backdrop '+c+'" />').appendTo(document.body),this.$element.on("click.dismiss.modal",a.proxy(function(a){a.target===a.currentTarget&&("static"==this.options.backdrop?this.$element[0].focus.call(this.$element[0]):this.hide.call(this))},this)),d&&this.$backdrop[0].offsetWidth,this.$backdrop.addClass("in"),!b)return;d?this.$backdrop.one(a.support.transition.end,b).emulateTransitionEnd(150):b()}else!this.isShown&&this.$backdrop?(this.$backdrop.removeClass("in"),a.support.transition&&this.$element.hasClass("fade")?this.$backdrop.one(a.support.transition.end,b).emulateTransitionEnd(150):b()):b&&b()};var c=a.fn.modal;a.fn.modal=function(c,d){return this.each(function(){var e=a(this),f=e.data("bs.modal"),g=a.extend({},b.DEFAULTS,e.data(),"object"==typeof c&&c);f||e.data("bs.modal",f=new b(this,g)),"string"==typeof c?f[c](d):g.show&&f.show(d)})},a.fn.modal.Constructor=b,a.fn.modal.noConflict=function(){return a.fn.modal=c,this},a(document).on("click.bs.modal.data-api",'[data-toggle="modal"]',function(b){var c=a(this),d=c.attr("href"),e=a(c.attr("data-target")||d&&d.replace(/.*(?=#[^\s]+$)/,"")),f=e.data("modal")?"toggle":a.extend({remote:!/#/.test(d)&&d},e.data(),c.data());b.preventDefault(),e.modal(f,this).one("hide",function(){c.is(":visible")&&c.focus()})}),a(document).on("show.bs.modal",".modal",function(){a(document.body).addClass("modal-open")}).on("hidden.bs.modal",".modal",function(){a(document.body).removeClass("modal-open")})}(window.jQuery),+function(a){"use strict";var b=function(a,b){this.type=this.options=this.enabled=this.timeout=this.hoverState=this.$element=null,this.init("tooltip",a,b)};b.DEFAULTS={animation:!0,placement:"top",selector:!1,template:'<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:!1,container:!1},b.prototype.init=function(b,c,d){this.enabled=!0,this.type=b,this.$element=a(c),this.options=this.getOptions(d);for(var e=this.options.trigger.split(" "),f=e.length;f--;){var g=e[f];if("click"==g)this.$element.on("click."+this.type,this.options.selector,a.proxy(this.toggle,this));else if("manual"!=g){var h="hover"==g?"mouseenter":"focus",i="hover"==g?"mouseleave":"blur";this.$element.on(h+"."+this.type,this.options.selector,a.proxy(this.enter,this)),this.$element.on(i+"."+this.type,this.options.selector,a.proxy(this.leave,this))}}this.options.selector?this._options=a.extend({},this.options,{trigger:"manual",selector:""}):this.fixTitle()},b.prototype.getDefaults=function(){return b.DEFAULTS},b.prototype.getOptions=function(b){return b=a.extend({},this.getDefaults(),this.$element.data(),b),b.delay&&"number"==typeof b.delay&&(b.delay={show:b.delay,hide:b.delay}),b},b.prototype.getDelegateOptions=function(){var b={},c=this.getDefaults();return this._options&&a.each(this._options,function(a,d){c[a]!=d&&(b[a]=d)}),b},b.prototype.enter=function(b){var c=b instanceof this.constructor?b:a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs."+this.type);return clearTimeout(c.timeout),c.hoverState="in",c.options.delay&&c.options.delay.show?(c.timeout=setTimeout(function(){"in"==c.hoverState&&c.show()},c.options.delay.show),void 0):c.show()},b.prototype.leave=function(b){var c=b instanceof this.constructor?b:a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs."+this.type);return clearTimeout(c.timeout),c.hoverState="out",c.options.delay&&c.options.delay.hide?(c.timeout=setTimeout(function(){"out"==c.hoverState&&c.hide()},c.options.delay.hide),void 0):c.hide()},b.prototype.show=function(){var b=a.Event("show.bs."+this.type);if(this.hasContent()&&this.enabled){if(this.$element.trigger(b),b.isDefaultPrevented())return;var c=this.tip();this.setContent(),this.options.animation&&c.addClass("fade");var d="function"==typeof this.options.placement?this.options.placement.call(this,c[0],this.$element[0]):this.options.placement,e=/\s?auto?\s?/i,f=e.test(d);f&&(d=d.replace(e,"")||"top"),c.detach().css({top:0,left:0,display:"block"}).addClass(d),this.options.container?c.appendTo(this.options.container):c.insertAfter(this.$element);var g=this.getPosition(),h=c[0].offsetWidth,i=c[0].offsetHeight;if(f){var j=this.$element.parent(),k=d,l=document.documentElement.scrollTop||document.body.scrollTop,m="body"==this.options.container?window.innerWidth:j.outerWidth(),n="body"==this.options.container?window.innerHeight:j.outerHeight(),o="body"==this.options.container?0:j.offset().left;d="bottom"==d&&g.top+g.height+i-l>n?"top":"top"==d&&g.top-l-i<0?"bottom":"right"==d&&g.right+h>m?"left":"left"==d&&g.left-h<o?"right":d,c.removeClass(k).addClass(d)}var p=this.getCalculatedOffset(d,g,h,i);this.applyPlacement(p,d),this.$element.trigger("shown.bs."+this.type)}},b.prototype.applyPlacement=function(a,b){var c,d=this.tip(),e=d[0].offsetWidth,f=d[0].offsetHeight,g=parseInt(d.css("margin-top"),10),h=parseInt(d.css("margin-left"),10);isNaN(g)&&(g=0),isNaN(h)&&(h=0),a.top=a.top+g,a.left=a.left+h,d.offset(a).addClass("in");var i=d[0].offsetWidth,j=d[0].offsetHeight;if("top"==b&&j!=f&&(c=!0,a.top=a.top+f-j),/bottom|top/.test(b)){var k=0;a.left<0&&(k=-2*a.left,a.left=0,d.offset(a),i=d[0].offsetWidth,j=d[0].offsetHeight),this.replaceArrow(k-e+i,i,"left")}else this.replaceArrow(j-f,j,"top");c&&d.offset(a)},b.prototype.replaceArrow=function(a,b,c){this.arrow().css(c,a?50*(1-a/b)+"%":"")},b.prototype.setContent=function(){var a=this.tip(),b=this.getTitle();a.find(".tooltip-inner")[this.options.html?"html":"text"](b),a.removeClass("fade in top bottom left right")},b.prototype.hide=function(){function b(){"in"!=c.hoverState&&d.detach()}var c=this,d=this.tip(),e=a.Event("hide.bs."+this.type);return this.$element.trigger(e),e.isDefaultPrevented()?void 0:(d.removeClass("in"),a.support.transition&&this.$tip.hasClass("fade")?d.one(a.support.transition.end,b).emulateTransitionEnd(150):b(),this.$element.trigger("hidden.bs."+this.type),this)},b.prototype.fixTitle=function(){var a=this.$element;(a.attr("title")||"string"!=typeof a.attr("data-original-title"))&&a.attr("data-original-title",a.attr("title")||"").attr("title","")},b.prototype.hasContent=function(){return this.getTitle()},b.prototype.getPosition=function(){var b=this.$element[0];return a.extend({},"function"==typeof b.getBoundingClientRect?b.getBoundingClientRect():{width:b.offsetWidth,height:b.offsetHeight},this.$element.offset())},b.prototype.getCalculatedOffset=function(a,b,c,d){return"bottom"==a?{top:b.top+b.height,left:b.left+b.width/2-c/2}:"top"==a?{top:b.top-d,left:b.left+b.width/2-c/2}:"left"==a?{top:b.top+b.height/2-d/2,left:b.left-c}:{top:b.top+b.height/2-d/2,left:b.left+b.width}},b.prototype.getTitle=function(){var a,b=this.$element,c=this.options;return a=b.attr("data-original-title")||("function"==typeof c.title?c.title.call(b[0]):c.title)},b.prototype.tip=function(){return this.$tip=this.$tip||a(this.options.template)},b.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".tooltip-arrow")},b.prototype.validate=function(){this.$element[0].parentNode||(this.hide(),this.$element=null,this.options=null)},b.prototype.enable=function(){this.enabled=!0},b.prototype.disable=function(){this.enabled=!1},b.prototype.toggleEnabled=function(){this.enabled=!this.enabled},b.prototype.toggle=function(b){var c=b?a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs."+this.type):this;c.tip().hasClass("in")?c.leave(c):c.enter(c)},b.prototype.destroy=function(){this.hide().$element.off("."+this.type).removeData("bs."+this.type)};var c=a.fn.tooltip;a.fn.tooltip=function(c){return this.each(function(){var d=a(this),e=d.data("bs.tooltip"),f="object"==typeof c&&c;e||d.data("bs.tooltip",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.tooltip.Constructor=b,a.fn.tooltip.noConflict=function(){return a.fn.tooltip=c,this}}(window.jQuery),+function(a){"use strict";var b=function(a,b){this.init("popover",a,b)};if(!a.fn.tooltip)throw new Error("Popover requires tooltip.js");b.DEFAULTS=a.extend({},a.fn.tooltip.Constructor.DEFAULTS,{placement:"right",trigger:"click",content:"",template:'<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'}),b.prototype=a.extend({},a.fn.tooltip.Constructor.prototype),b.prototype.constructor=b,b.prototype.getDefaults=function(){return b.DEFAULTS},b.prototype.setContent=function(){var a=this.tip(),b=this.getTitle(),c=this.getContent();a.find(".popover-title")[this.options.html?"html":"text"](b),a.find(".popover-content")[this.options.html?"html":"text"](c),a.removeClass("fade top bottom left right in"),a.find(".popover-title").html()||a.find(".popover-title").hide()},b.prototype.hasContent=function(){return this.getTitle()||this.getContent()},b.prototype.getContent=function(){var a=this.$element,b=this.options;return a.attr("data-content")||("function"==typeof b.content?b.content.call(a[0]):b.content)},b.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".arrow")},b.prototype.tip=function(){return this.$tip||(this.$tip=a(this.options.template)),this.$tip};var c=a.fn.popover;a.fn.popover=function(c){return this.each(function(){var d=a(this),e=d.data("bs.popover"),f="object"==typeof c&&c;e||d.data("bs.popover",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.popover.Constructor=b,a.fn.popover.noConflict=function(){return a.fn.popover=c,this}}(window.jQuery),+function(a){"use strict";function b(c,d){var e,f=a.proxy(this.process,this);this.$element=a(c).is("body")?a(window):a(c),this.$body=a("body"),this.$scrollElement=this.$element.on("scroll.bs.scroll-spy.data-api",f),this.options=a.extend({},b.DEFAULTS,d),this.selector=(this.options.target||(e=a(c).attr("href"))&&e.replace(/.*(?=#[^\s]+$)/,"")||"")+" .nav li > a",this.offsets=a([]),this.targets=a([]),this.activeTarget=null,this.refresh(),this.process()}b.DEFAULTS={offset:10},b.prototype.refresh=function(){var b=this.$element[0]==window?"offset":"position";this.offsets=a([]),this.targets=a([]);var c=this;this.$body.find(this.selector).map(function(){var d=a(this),e=d.data("target")||d.attr("href"),f=/^#\w/.test(e)&&a(e);return f&&f.length&&[[f[b]().top+(!a.isWindow(c.$scrollElement.get(0))&&c.$scrollElement.scrollTop()),e]]||null}).sort(function(a,b){return a[0]-b[0]}).each(function(){c.offsets.push(this[0]),c.targets.push(this[1])})},b.prototype.process=function(){var a,b=this.$scrollElement.scrollTop()+this.options.offset,c=this.$scrollElement[0].scrollHeight||this.$body[0].scrollHeight,d=c-this.$scrollElement.height(),e=this.offsets,f=this.targets,g=this.activeTarget;if(b>=d)return g!=(a=f.last()[0])&&this.activate(a);for(a=e.length;a--;)g!=f[a]&&b>=e[a]&&(!e[a+1]||b<=e[a+1])&&this.activate(f[a])},b.prototype.activate=function(b){this.activeTarget=b,a(this.selector).parents(".active").removeClass("active");var c=this.selector+'[data-target="'+b+'"],'+this.selector+'[href="'+b+'"]',d=a(c).parents("li").addClass("active");d.parent(".dropdown-menu").length&&(d=d.closest("li.dropdown").addClass("active")),d.trigger("activate")};var c=a.fn.scrollspy;a.fn.scrollspy=function(c){return this.each(function(){var d=a(this),e=d.data("bs.scrollspy"),f="object"==typeof c&&c;e||d.data("bs.scrollspy",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.scrollspy.Constructor=b,a.fn.scrollspy.noConflict=function(){return a.fn.scrollspy=c,this},a(window).on("load",function(){a('[data-spy="scroll"]').each(function(){var b=a(this);b.scrollspy(b.data())})})}(window.jQuery),+function(a){"use strict";var b=function(b){this.element=a(b)};b.prototype.show=function(){var b=this.element,c=b.closest("ul:not(.dropdown-menu)"),d=b.attr("data-target");if(d||(d=b.attr("href"),d=d&&d.replace(/.*(?=#[^\s]*$)/,"")),!b.parent("li").hasClass("active")){var e=c.find(".active:last a")[0],f=a.Event("show.bs.tab",{relatedTarget:e});if(b.trigger(f),!f.isDefaultPrevented()){var g=a(d);this.activate(b.parent("li"),c),this.activate(g,g.parent(),function(){b.trigger({type:"shown.bs.tab",relatedTarget:e})})}}},b.prototype.activate=function(b,c,d){function e(){f.removeClass("active").find("> .dropdown-menu > .active").removeClass("active"),b.addClass("active"),g?(b[0].offsetWidth,b.addClass("in")):b.removeClass("fade"),b.parent(".dropdown-menu")&&b.closest("li.dropdown").addClass("active"),d&&d()}var f=c.find("> .active"),g=d&&a.support.transition&&f.hasClass("fade");g?f.one(a.support.transition.end,e).emulateTransitionEnd(150):e(),f.removeClass("in")};var c=a.fn.tab;a.fn.tab=function(c){return this.each(function(){var d=a(this),e=d.data("bs.tab");e||d.data("bs.tab",e=new b(this)),"string"==typeof c&&e[c]()})},a.fn.tab.Constructor=b,a.fn.tab.noConflict=function(){return a.fn.tab=c,this},a(document).on("click.bs.tab.data-api",'[data-toggle="tab"], [data-toggle="pill"]',function(b){b.preventDefault(),a(this).tab("show")})}(window.jQuery),+function(a){"use strict";var b=function(c,d){this.options=a.extend({},b.DEFAULTS,d),this.$window=a(window).on("scroll.bs.affix.data-api",a.proxy(this.checkPosition,this)).on("click.bs.affix.data-api",a.proxy(this.checkPositionWithEventLoop,this)),this.$element=a(c),this.affixed=this.unpin=null,this.checkPosition()};b.RESET="affix affix-top affix-bottom",b.DEFAULTS={offset:0},b.prototype.checkPositionWithEventLoop=function(){setTimeout(a.proxy(this.checkPosition,this),1)},b.prototype.checkPosition=function(){if(this.$element.is(":visible")){var c=a(document).height(),d=this.$window.scrollTop(),e=this.$element.offset(),f=this.options.offset,g=f.top,h=f.bottom;"object"!=typeof f&&(h=g=f),"function"==typeof g&&(g=f.top()),"function"==typeof h&&(h=f.bottom());var i=null!=this.unpin&&d+this.unpin<=e.top?!1:null!=h&&e.top+this.$element.height()>=c-h?"bottom":null!=g&&g>=d?"top":!1;this.affixed!==i&&(this.unpin&&this.$element.css("top",""),this.affixed=i,this.unpin="bottom"==i?e.top-d:null,this.$element.removeClass(b.RESET).addClass("affix"+(i?"-"+i:"")),"bottom"==i&&this.$element.offset({top:document.body.offsetHeight-h-this.$element.height()}))}};var c=a.fn.affix;a.fn.affix=function(c){return this.each(function(){var d=a(this),e=d.data("bs.affix"),f="object"==typeof c&&c;e||d.data("bs.affix",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.affix.Constructor=b,a.fn.affix.noConflict=function(){return a.fn.affix=c,this},a(window).on("load",function(){a('[data-spy="affix"]').each(function(){var b=a(this),c=b.data();c.offset=c.offset||{},c.offsetBottom&&(c.offset.bottom=c.offsetBottom),c.offsetTop&&(c.offset.top=c.offsetTop),b.affix(c)})})}(window.jQuery);;angular.module('reddonor').run(['$templateCache', function($templateCache) {

  $templateCache.put('views/partials/templates/about-blood.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>About Blood</h1>\n" +
    "        <h2>Blood is the fluid that transports oxygen and nutrients around the body. An average adult has just under 5 litres of blood circulating around their body.</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row\">\n" +
    "    <aboutnav></aboutnav>\n" +
    "  </div>\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h2 class=\"title\"> Blood Types</h2>\n" +
    "      <div class=\"introduction\">\n" +
    "        <h2>Knowing your blood type is important. When there is a critical need for a specific blood type you can be ready to answer the call.</h2>\n" +
    "      </div>\n" +
    "      <!--img.img-responsive(src=\"/assets/images/gallery/blood-types.jpg\").thumbnail.pull-right-->\n" +
    "      <h4>THE MAIN BLOOD GROUPS</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Blood consists of many major group systems, but the most important system in blood donation is the ABO Blood Group System. This system consists of:</p>\n" +
    "        <p>Group O – has neither A nor B antigens on red cells</p>\n" +
    "        <p>Group A – has only the A antigen on red cells</p>\n" +
    "        <p>Group B – has only the B antigen on red cells</p>\n" +
    "        <p>Group AB – has both A and B antigens on red cells</p>\n" +
    "        <p>Each blood group is also classified by either a Rh factor of Rh negative (-) or Rh positive. (+)</p>\n" +
    "      </div>\n" +
    "      <h4>BLOOD RECIPIENTS & DONORS</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Blood Type O- can donate to anyone. It is the Universal Donor.</p>\n" +
    "        <p>Blood Type O+ can donate to O+, A+, B+ and AB+.</p>\n" +
    "        <p>Blood Type A+ can donate to A+ and AB+.</p>\n" +
    "        <p>Blood Type A- can donate to A+, A-, AB+ and AB-.</p>\n" +
    "        <p>Blood Type B+ can donate to B+ and AB+.</p>\n" +
    "        <p>Blood Type B- can donate to B+, B-, AB+ and AB-.</p>\n" +
    "        <p>Blood Type AB+ can donate to AB+, but can receive from anyone. It is the Universal Recipient.</p>\n" +
    "        <p>Blood Type AB- can donate to AB+ and AB-.</p>\n" +
    "      </div>\n" +
    "      <h4>BLOOD GROUP INHERITANCE</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Blood groups are inherited from your parents. The four blood groups, O, A, B and AB are controlled by the A, B and O genes. The A and B genes are dominant and group O is dependent on the inheritance of an O gene from each parent.</p>\n" +
    "        <p>The inheritance of the Rhesus D (Rh D)gene from either or both parents results in the Rh D positive blood group “expression”. The absence of the Rh D gene results in the expression of the Rh D negative blood group.</p>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/aboutblood-nav.html',
    "\n" +
    "<div class=\"col-md-10 col-md-offset-1\">\n" +
    "  <div class=\"submenu hidden-xs\">\n" +
    "    <ul>\n" +
    "      <li><a href=\"/aboutblood/bloodtypes\">Blood Types</a></li><i class=\"fa fa-heart\"></i>\n" +
    "      <li><a href=\"/aboutblood/safety\">Blood Safety</a></li><i class=\"fa fa-heart\"></i>\n" +
    "      <li><a href=\"/aboutblood/uses\">Uses of Blood</a></li><i class=\"fa fa-heart\"></i>\n" +
    "      <li><a href=\"/aboutblood/faqs\">FAQs</a></li>\n" +
    "    </ul>\n" +
    "  </div>\n" +
    "  <div class=\"submenu-small visible-xs\">\n" +
    "    <ul>\n" +
    "      <li><a href=\"/aboutblood/bloodtypes\">Blood Types</a></li>\n" +
    "      <li><a href=\"/aboutblood/safety\">Blood Safety</a></li>\n" +
    "      <li><a href=\"/aboutblood/uses\">Uses of Blood</a></li>\n" +
    "      <li><a href=\"/aboutblood/faqs\">FAQs</a></li>\n" +
    "    </ul>\n" +
    "  </div>\n" +
    "</div>"
  );


  $templateCache.put('views/partials/templates/aboutus.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>About Us</h1>\n" +
    "        <h2>Red Donor is a social enterprise devoted to increasing the capacity of a safe and constant supply of blood for every member of the community.</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h4>Our Story</h4>\n" +
    "      <p>Samora Reid has always been passionate about two things - technology and how information helps us understand how things work. These two passions have joined with his experiences with blood donation to create a movement for a better world.</p>\n" +
    "      <p>In 2006, he went to donate blood for the first time for his late grandfather. Like most people, after the task was done he returned to his regular routine without giving it a second thought. Within the following years, Samora would be exposed to the frightening experience of the importance of blood in saving the life of the father of a friend.</p>\n" +
    "      <p>Months later, he realised many families must go through that same experience, wondering if there is enough blood to save the life of their loved ones. In September of 2012, Samora started building a web application to connect blood donors to those who needed the vital commodity. Inspired by the culture of the Caribbean that idea grew into the founding of Red Donor. An organisation aimed at utilising the close-knit community spirit of the Caribbean infused with innovative technological and creative solutions to encourage more people to donate blood.</p>\n" +
    "      <p>Red Donor's approach to achieving this goal is by observing the behaviours and trends within the community and analysing that data to create more altrustic donor communities within the region.</p><br/>\n" +
    "      <h4>Our Team</h4>\n" +
    "      <div style=\"text-align: center;\" class=\"row\">\n" +
    "        <div class=\"col-md-6\"><img src=\"http://org.reddonor.com/wp-content/themes/Red_Donor_BWP/img/samorareid.png\" alt=\"image\"/>\n" +
    "          <h4>Samora Reid</h4>\n" +
    "          <h5>Founder</h5>\n" +
    "          <p>Samora is a freelance web developer. His experiences include building websites for several industries. Red Donor is his first foray into the world of social entrepreneurship and changing the world through technology.</p>\n" +
    "          <p>In his spare time he can be found listening to music or playing videogames with a cup of coffee nearby.</p>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-6\"> <img src=\"http://org.reddonor.com/wp-content/themes/Red_Donor_BWP/img/joshuaclarke.png\" alt=\"image\"/>\n" +
    "          <h4>Joshua Clarke</h4>\n" +
    "          <h5>Creative Director</h5>\n" +
    "          <p>Joshua Clarke was always found with a pencil in hand doodling on some artwork. In his final year of a Graphic Design Bsc and a regular exhibitor at AnimeKon. Joshua uses non-traditional artistic inspiration to fuel his advertising design strategies.</p>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/blood-faqs.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>About Blood</h1>\n" +
    "        <h2>Blood is the fluid that transports oxygen and nutrients around the body. An average adult has just under 5 litres of blood circulating around their body.</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row\">\n" +
    "    <aboutnav></aboutnav>\n" +
    "  </div>\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h2 class=\"title\">FAQs</h2>\n" +
    "      <div class=\"introduction\">\n" +
    "        <h2>Answers to your questions about blood and the blood donation process.</h2>\n" +
    "      </div>\n" +
    "      <h4>WHAT ARE THE DIFFERENT BLOOD TYPES?</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>There are two major blood type systems, the ABO system and the Rhesus (Rh factor) system. These blood systems created the 8 major blood types – O+, O-, A+, A-, B+, B-, AB+ and AB-.</p>\n" +
    "      </div>\n" +
    "      <h4>WHAT ARE THE DIFFERENT COMPONENTS OF BLOOD AND WHAT DO THEY DO?</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <h4>Red Cells</h4>\n" +
    "        <p>The main function of red blood cells is to distribute oxygen to body tissues and to carry waste carbon dioxide back to the lungs. These are used in the treatment of anaemia, cancer, premature newborns, sickle cell disease and to replace lost blood in accidents, surgery and after childbirth.</p>\n" +
    "        <p>Red cells are stored in a refrigerator and have a shelf life of up to 42 days.</p>\n" +
    "        <h4>Plasma</h4>\n" +
    "        <p>Plasma is the straw coloured fluid in which the red cells, white cells and platelets are suspended. Plasma is the most versatile component of blood as it can be processed into a variety of products and each product can be used to treat a number of potentially life-threatening conditions.</p>\n" +
    "        <p>Plasma is stored frozen and has a shelf life of up to 12 months.</p>\n" +
    "        <h4>Platelets</h4>\n" +
    "        <p>Platelets are components of blood that assist in the blood clotting process.</p>\n" +
    "      </div>\n" +
    "      <h4>HOW MUCH BLOOD IS TAKEN?</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>A single unit of blood taken during a whole blood donation is about 470mL (about a pint). This amount is less than 10% of your total blood volume. Your body keeps on replenishing blood all the time whether you give blood or not, so this amount is quickly replaced.</p>\n" +
    "      </div>\n" +
    "      <h4>HOW MUCH BLOOD DOES AN ADULT BODY HAVE?</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>An average size adult has a blood volume of about 5 litres.</p>\n" +
    "      </div>\n" +
    "      <h4>WHAT HAPPENS TO THE BLOOD ONCE I’VE DONATED?</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Whole blood donations are separated into their components (red cells, platelets and plasma). After processing, red cells are refrigerated and can be stored for up to 42 days. Platelets are stored at room temperature and can be stored for up to 5 days. Plasma is frozen and can be stored for up to 12 months.</p>\n" +
    "        <p>Whilst a significant proportion of the plasma is used for direct transfusion to patients, the majority of donated plasma is further processed into a number of very important plasma products. These plasma products include immunisations against chicken pox, hepatitis B and tetanus; clotting factors for the treatment of patients with haemophilia; protein products for the treatment of patients with burns, liver and kidney diseases; and immunoglobulin products for the treatment of patients with antibody deficiencies and other disorders of the immune system.</p>\n" +
    "      </div>\n" +
    "      <h4>HOW LONG UNTIL MY BLOOD IS USED?</h4>\n" +
    "      <div class=\"blurb\">  \n" +
    "        <p>All blood donations are tested and processed and available for use between 24 and 48 hours after collection. Whole blood is separated into its components (red cells, platelets, plasma). After processing, red cells can be stored for up to 42 days; plasma is frozen and can be stored for up to 12 months; and platelets have a shelf-life of only five days.</p>\n" +
    "      </div>\n" +
    "      <h4>WHAT IS MY BLOOD TESTED FOR?</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>After donation, all blood is tested at every donation for blood type, the presence of red cell antibodies and for the following infections: HIV1 & 2, hepatitis B & C, HTLV I & II and syphilis. Some donations are also tested for malaria depending on the donor’s answers to the questions on the donor questionnaire. </p>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/blood-safety.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>About Blood</h1>\n" +
    "        <h2>Blood is the fluid that transports oxygen and nutrients around the body. An average adult has just under 5 litres of blood circulating around their body.</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row\">\n" +
    "    <aboutnav></aboutnav>\n" +
    "  </div>\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h2 class=\"title\">Blood Safety</h2>\n" +
    "      <div class=\"introduction\">\n" +
    "        <h2>Several guidelines and testing procedures exist to ensure the safety of donors, recipients and the blood supply.</h2>\n" +
    "      </div>\n" +
    "      <h4>DONOR SAFETY</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>The mini physical performed by the Blood Collecting Centre before your blood donation is designed to help them spot potential problems. Sterile equipment and best practices are utilised to reduce any potential risk of infection.</p>\n" +
    "      </div>\n" +
    "      <h4>RECIPIENT SAFETY</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Blood Collecting Centres perform a range of laboratory tests on every unit of blood donated. The tests include:</p>\n" +
    "        <p>ABO Blood Type</p>\n" +
    "        <p>Rh Groups (Positive or Negative)</p>\n" +
    "        <p>HIV/AIDS</p>\n" +
    "        <p>Hepatitis B & C</p>\n" +
    "        <p>Syphilis</p>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/blood-uses.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>About Blood</h1>\n" +
    "        <h2>Blood is the fluid that transports oxygen and nutrients around the body. An average adult has just under 5 litres of blood circulating around their body.</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row\">\n" +
    "    <aboutnav></aboutnav>\n" +
    "  </div>\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h2 class=\"title\">Uses of Blood</h2>\n" +
    "      <div class=\"introduction\">\n" +
    "        <h2>Blood collected is not just used for emergency operations or accident victims. Many patients could not survive without blood transfusions.</h2>\n" +
    "      </div>\n" +
    "      <h4>HOW DONATED BLOOD IS USED</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>The blood collected is separated into its 3 main components:</p>\n" +
    "        <p>Red Cells</p>\n" +
    "        <p>Plasma</p>\n" +
    "        <p>Platelets</p>\n" +
    "      </div>\n" +
    "      <h4>RED CELLS</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>The main function of red blood cells is to distribute oxygen to body tissues and to carry waste carbon dioxide back to the lungs.</p>\n" +
    "        <p>These are used in the treatment of anaemia, cancer, premature newborns, sickle cell disease and to replace lost blood in accidents, surgery and after childbirth.</p>\n" +
    "      </div>\n" +
    "      <h4>PLASMA</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Fresh frozen plasma is used after obstetric loss of blood (which is usually childbirth), during cardiac surgery, and to reverse any anti-coagulant treatment. It is also used to replace clotting factors after massive transfusions or when they are not being sufficiently produced, such as liver disease.</p>\n" +
    "        <p>Processed plasma has several important uses. It is used in the treatment of haemophilia and for treating sufferers of Christmas disease, a life-threatening form of haemophilia. Processed plasma is also used to help produce stronger antibodies against diseases like tetanus, hepatitis, chickenpox and rabies. It also helps generate anti-D, which is used for RhD negative pregnant women carrying RhD positive babies. Additionally there is a protein called albumin contained in plasma, which is extremely beneficial for burn victims.</p>\n" +
    "      </div>\n" +
    "      <h4>PLATELETS</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Platelets can be used in bone marrow failure, post transplant and chemotherapy treatments, and leukaemia. Platelets can be of huge benefit to the recipient.</p>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/contact.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>Contact Us</h1>\n" +
    "        <h2>We're open to any questions, concerns, comments or suggestions you may have. Feel free to reach out to us via email or social media.\t\t\t\t</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <div class=\"blurb\">\n" +
    "        <h4>To Report Problems with the Website</h4>\n" +
    "        <p><a href=\"mailto:support@reddonor.com\">support@reddonor.com</a></p>\n" +
    "        <h4>For Questions Or Suggestions about the Organisation</h4>\n" +
    "        <p><a href=\"mailto:sreid@reddonor.com\">sreid@reddonor.com</a></p>\n" +
    "        <h4>Social</h4>\n" +
    "        <p>Like us on <a href=\"https://www.facebook.com/RedDonor\" target=\"_blank\">Facebook</a></p>\n" +
    "        <p>Follow us on <a href=\"https://twitter.com/RedDonor\" target=\"_blank\">Twitter</a></p>\n" +
    "        <p>Follow us on <a href=\"https://plus.google.com/u/0/b/110993637022991434407/110993637022991434407/posts\" target=\"_blank\">Google+              </a></p>\n" +
    "        <p>Subscribe on <a href=\"http://www.youtube.com/RedDonor\" target=\"_blank\">YouTube</a></p>\n" +
    "        <p>Join us on <a href=\"http://www.linkedin.com/company/red-donor\" target=\"_blank\">Linkedin</a></p>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/eligibility.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>Give Blood</h1>\n" +
    "        <h2>Blood is a vital lifeline to saving and preserving hundreds of lives daily. Less than 2% of the population donates blood.</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row\">\n" +
    "    <givenav></givenav>\n" +
    "  </div>\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h2 class=\"title\">ELIGIBILITY FOR BLOOD DONATION</h2>\n" +
    "      <div class=\"introduction\">\n" +
    "        <h2>Most people can give blood, but to ensure the safety of blood donors and recipients, all blood donors must match the eligibility criteria.</h2>\n" +
    "      </div>\n" +
    "      <h5>NOTICE</h5>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>The eligibility criteria for blood donors differs from country to country. Therefore blood donations centres in your country may have a different set of requirements.</p>\n" +
    "      </div>\n" +
    "      <h4>YOU CAN GIVE BLOOD IF YOU:</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <ul>\n" +
    "          <li>are between the ages of 18 and 65 and in healthy condition</li>\n" +
    "          <li>weigh more than 110 pounds</li>\n" +
    "          <li>have not had a tattoo or body piercing within the past 12 months</li>\n" +
    "          <li>are not pregnant or nursing</li>\n" +
    "          <li>have no history of jaundice after age 11</li>\n" +
    "        </ul><br/>\n" +
    "        <p>Diabetics are eligible if they are controlled by diet or oral medications, no insulin</p>\n" +
    "        <p>Persons with hypertension are eligible as long as their blood pressure is within a reasonable reading at the time of donation</p>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/faqs.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>FAQs</h1>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h4>What is Red Donor?</h4>\n" +
    "      <p>\n" +
    "        Red Donor is a crowd sourcing platform for blood donations. People from around the Caribbean can post requests for blood and pledge to donate \n" +
    "        blood to people who need it in their communities.\n" +
    "      </p>\n" +
    "      <h4>How does it work?</h4>\n" +
    "      <p>\n" +
    "        Blood donations are pledged via Blood Drives. A Blood Drive can be created by or on behalf of the blood recipient by \n" +
    "        any person. Blood donors with a compatible blood type can pledge to make a donation at their nearest blood donation centre.\n" +
    "      </p>\n" +
    "      <h4>How to pledge a blood donation?</h4>\n" +
    "      <p>After signing up for a Red Donor account, you can browse the active Blood Drives for recipients with compatible blood types.</p>\n" +
    "      <h4>How to donate blood?</h4>\n" +
    "      <p>You can donate at your nearest local blood donation clinic. The opening hours of the local blood donor centres are listed with Blood Drive details.</p>\n" +
    "      <p>The process of donating blood encompasses 3 steps - Preparation for Your Donation, The Donation Process and After the Donation.</p>\n" +
    "      <h5>Preparation For Your Donation</h5>\n" +
    "      <ul>\n" +
    "        <li>The day before you donate, drink plenty of liquids</li>\n" +
    "        <li>Eat something at least 3 hours before you donate</li>\n" +
    "      </ul>\n" +
    "      <h5>Donation Process</h5>\n" +
    "      <ul>\n" +
    "        <li>Before donation you will be asked a few questions to determine if you are eligible to donate blood</li>\n" +
    "        <li>Your hemoglobin level, temperature, pulse and blood will be checked before donation</li>\n" +
    "        <li>Sterile equipment will be used to collect the blood for about 10 minutes</li>\n" +
    "        <li>After donating, you will be asked to rest in the chair for a few minutes during which you can request a drink</li>\n" +
    "      </ul>\n" +
    "      <h5>After Donating</h5>\n" +
    "      <ul>\n" +
    "        <li>Continue to drink water throughout the day</li>\n" +
    "        <li>Try not to over exert yourself for the rest of the day</li>\n" +
    "      </ul>\n" +
    "      <h4>How to start a Blood Drive?</h4>\n" +
    "      <p>\n" +
    "        Click Create A Blood Drive. You can choose to create a blood drive as a guest or sign up for an account first. Type the recipient's name, a description of the reason for the Blood Drive, \n" +
    "        blood type required, country located and the deadline date to start a Blood Drive.\n" +
    "      </p>\n" +
    "      <h4>How to find persons to donate to?</h4>\n" +
    "      <p>The quickest way to find someone to donate to is to sign up for a Red Donor account and click Compatible Drives.</p>\n" +
    "      <h4>Who can donate blood?</h4>\n" +
    "      <p>The eligibility for blood donors differs from country to country. Most people are able to donate blood if they: </p>\n" +
    "      <ul>\n" +
    "        <li>are between the ages of 18 and 65 and in healthy condition</li>\n" +
    "        <li>weigh more than 110 pounds</li>\n" +
    "        <li>have not had a tattoo or body piercing within the past 12 months</li>\n" +
    "        <li>are not pregnant or nursing</li>\n" +
    "        <li>have no history of jaundice after age 11</li>\n" +
    "      </ul>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/giveblood-nav.html',
    "\n" +
    "<div class=\"col-md-10 col-md-offset-1\">\n" +
    "  <div class=\"submenu hidden-xs\">\n" +
    "    <ul>\n" +
    "      <li><a href=\"/giveblood/whydonate\">Why Donate</a></li><i class=\"fa fa-heart\"></i>\n" +
    "      <li><a href=\"/giveblood/eligibility\">Eligibility</a></li><i class=\"fa fa-heart\"></i>\n" +
    "      <li><a href=\"/giveblood/procedure\">Donation Procedure</a></li><i class=\"fa fa-heart\"></i>\n" +
    "      <li><a href=\"/giveblood/tips\">Tips</a></li>\n" +
    "    </ul>\n" +
    "  </div>\n" +
    "  <div class=\"submenu-small visible-xs\">\n" +
    "    <ul>\n" +
    "      <li><a href=\"/giveblood/whydonate\">Why Donate</a></li>\n" +
    "      <li><a href=\"/giveblood/eligibility\">Eligibility</a></li>\n" +
    "      <li><a href=\"/giveblood/procedure\">The Procedure</a></li>\n" +
    "      <li><a href=\"/giveblood/tips\">Tips</a></li>\n" +
    "    </ul>\n" +
    "  </div>\n" +
    "</div>"
  );


  $templateCache.put('views/partials/templates/home.html',
    "\n" +
    "<div class=\"hero-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"hero-section\">\n" +
    "      <h1>Give the gift of life!</h1>\n" +
    "      <p>Someone needs your help today. We can save lives, one donation at a time.</p>\n" +
    "      <p><a href=\"/savealife\" class=\"herobtn\">How To Save A Life?</a>\n" +
    "        <!--a.herobtn(href='http://my.reddonor.com') COMING SOON-->\n" +
    "      </p>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>\n" +
    "<section class=\"container introduction\">\n" +
    "  <div class=\"row\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h2>Red Donor is a social enterprise devoted to increasing the capacity of a safe and constant supply of blood for every member of the community.</h2>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "  <div class=\"row\">\n" +
    "    <div class=\"col-md-4\"><img src=\"assets/images/bloodcrisis-md.jpg\" alt=\"Help Save A life\" title=\"&copy; Photo credit: Jason Hargrove - flickr.com/photos/salty_soul/4165610216\" class=\"img-rounded img-responsive hidden-sm hidden-xs\"/><img src=\"assets/images/bloodcrisis-sm.jpg\" alt=\"Help Save A life\" title=\"&copy; Photo credit: Jason Hargrove - flickr.com/photos/salty_soul/4165610216\" class=\"img-rounded img-responsive visible-sm visible-xs\"/>\n" +
    "      <h4><a href=\"/giveblood\">Help Save A Life</a></h4>\n" +
    "      <p>Donate blood and help save the life of someone in your community. 1 donation of blood can save up to 3 lives.</p>\n" +
    "    </div>\n" +
    "    <div class=\"col-md-4\"><img src=\"assets/images/helpotherblood-md.jpg\" alt=\"Start A Blood Drive\" title=\"&copy; Photo credit: thirteenofclubs - flickr.com/photos/thirteenofclubs/4216252051\" class=\"img-rounded img-responsive hidden-sm hidden-xs\"/><img src=\"assets/images/helpotherblood-sm.jpg\" alt=\"Start A Blood Drive\" title=\"&copy; Photo credit: makelessnoise - flickr.com/photos/thirteenofclubs/4216252051\" class=\"img-rounded img-responsive visible-sm visible-xs\"/>\n" +
    "      <h4><a href=\"/howitworks\">How To Start A Blood Drive</a></h4>\n" +
    "      <p>Join to movement to encourage more people, organisations and business to donate for a worthy cause.</p>\n" +
    "    </div>\n" +
    "    <div class=\"col-md-4\"><img src=\"assets/images/youeligible-md.jpg\" alt=\"Learn About Blood\" title=\"&copy; Photo credit: European Parliament - flickr.com/photos/european_parliament/5933370325\" class=\"img-rounded img-responsive hidden-sm hidden-xs\"/><img src=\"assets/images/youeligible-sm.jpg\" alt=\"Learn About Blood\" title=\"&copy; Photo credit: European Parliament - flickr.com/photos/european_parliament/5933370325\" class=\"img-rounded img-responsive visible-sm visible-xs\"/>\n" +
    "      <h4><a href=\"/aboutblood\">Learn about Blood</a></h4>\n" +
    "      <p>1 in 3 people will need blood in their lifetime and less than 2% of the eligible donors in the community donate blood.</p>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h2>Every day hundreds of people are in need of blood. Pledge to donate. You can help save a life.</h2>\n" +
    "        <div class=\"col-sm-6\"><a href=\"/aboutblood\" class=\"regbtn\">Learn About Blood Donation </a></div>\n" +
    "        <div class=\"col-sm-6\"><a href=\"http://my.reddonor.com/create\" class=\"regbtn\">Start a Blood Drive</a></div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/howitworks.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>How It Works</h1>\n" +
    "        <h2>...</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-12\">\n" +
    "      <div class=\"row\">\n" +
    "        <div class=\"col-md-4\">\n" +
    "          <div class=\"works\">\n" +
    "            <h3 class=\"title\">For Blood Requesters</h3>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/join2.png\" alt=\"Join Red Donor\"/>\n" +
    "              <h3>Join Red Donor</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Simple Sign Up </p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/create.png\" alt=\"Create a blood drive\"/>\n" +
    "              <h3>Create a Blood Drive</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Make a request for blood</p>\n" +
    "                <p>Set a donation period </p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/receive.png\" alt=\"Receive pledges to donate\"/>\n" +
    "              <h3>Receive Pledges to Donate</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Willing Blood Donors Pledge to Donate</p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/pledged.png\" alt=\"Receive confirmation\"/>\n" +
    "              <h3>Receive Confirmation</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Blood Donors confirm donation at local collection centre</p>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "          <div class=\"works\">\n" +
    "            <h3 class=\"title\">For Blood Donors</h3>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/join2.png\" alt=\"Join Red Donor\"/>\n" +
    "              <h3>Join Red Donor</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Simple Sign Up </p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/find.png\" alt=\"Find Blood Drives\"/>\n" +
    "              <h3>Find Blood Drives</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Locate local persons uregently in need of blood </p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/pledge.png\" alt=\"Pledge to donate\"/>\n" +
    "              <h3>Pledge to Donate</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Make a pledge to donate blood at your local donation centre</p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/confirm.png\" alt=\"Confirm Donation\"/>\n" +
    "              <h3>Confirm Donation</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Notify the Blood Requester of Your Donation</p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/happy.png\" alt=\"Feel Good\"/>\n" +
    "              <h3>Feel Good for Saving A Life</h3><br/>\n" +
    "              <div class=\"st-text\"></div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "          <div class=\"works\">\n" +
    "            <h3 class=\"title\">For Organisations</h3>\n" +
    "            <div class=\"st-highlight\">\n" +
    "              <h3>Coming Soon</h3><br/>\n" +
    "              <div class=\"st-text\"></div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/howtosave.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>How To Save A Life?</h1>\n" +
    "        <h2>Red Donor takes the stress out of finding willing blood donors for your loved ones and connects blood donors to those who need their help.</h2>\n" +
    "        <!--h2 Through education and innovation we want to ensure a safe and stable supply of blood for your community.-->\n" +
    "        <div class=\"row\">\n" +
    "          <div class=\"col-md-8 col-md-offset-2\">\n" +
    "            <div class=\"video-container\">\n" +
    "              <iframe width=\"560\" height=\"315\" src=\"//www.youtube.com/embed/9oCR1Yo5em8?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;wmode=transparent\" frameborder=\"0\"></iframe>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"row\">\n" +
    "          <div class=\"col-md-10 col-md-offset-1\">\n" +
    "            <h4>Red Donor allows persons to make public requests for blood for their family, friends and loved ones. People who have signed up for an account can pledge to donate blood to the requested blood donation centre. \t\t\t\t</h4>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-12\">\n" +
    "      <h2>Quick Use Instructions </h2>\n" +
    "      <div class=\"row\">\n" +
    "        <div class=\"col-md-4 col-md-offset-2\">\n" +
    "          <div class=\"works\">\n" +
    "            <h3 class=\"title\">For Blood Requesters</h3>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/join2.png\" alt=\"Join Red Donor\"/>\n" +
    "              <h3>Join Red Donor</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Simple Sign Up </p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/create.png\" alt=\"Create a blood drive\"/>\n" +
    "              <h3>Create a Blood Drive</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Make a request for blood</p>\n" +
    "                <p>Set a donation period </p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/receive.png\" alt=\"Receive pledges to donate\"/>\n" +
    "              <h3>Receive Pledges to Donate</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Willing Blood Donors Pledge to Donate</p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/pledged.png\" alt=\"Receive confirmation\"/>\n" +
    "              <h3>Receive Confirmation</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Blood Donors confirm donation at local collection centre</p>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "          <div class=\"works\">\n" +
    "            <h3 class=\"title\">For Blood Donors</h3>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/join2.png\" alt=\"Join Red Donor\"/>\n" +
    "              <h3>Join Red Donor</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Simple Sign Up </p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/find.png\" alt=\"Find Blood Drives\"/>\n" +
    "              <h3>Find Blood Drives</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Locate local persons uregently in need of blood </p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/pledge.png\" alt=\"Pledge to donate\"/>\n" +
    "              <h3>Pledge to Donate</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Make a pledge to donate blood at your local donation centre</p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/confirm.png\" alt=\"Confirm Donation\"/>\n" +
    "              <h3>Confirm Donation</h3><br/>\n" +
    "              <div class=\"st-text\">\n" +
    "                <p>Notify the Blood Requester of Your Donation</p><i class=\"fa fa-chevron-down fa-5x rred\"></i>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"st-highlight\"><img src=\"/assets/images/works/happy.png\" alt=\"Feel Good\"/>\n" +
    "              <h3>Feel Good for Saving A Life</h3><br/>\n" +
    "              <div class=\"st-text\"></div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div><!--.col-md-4\n" +
    "        <div class=\"works\">\n" +
    "          <h3 class=\"title\">For Organisations</h3>\n" +
    "          <div class=\"st-highlight\">\n" +
    "            <h3>Coming Soon</h3><br/>\n" +
    "            <div class=\"st-text\"></div>\n" +
    "          </div>\n" +
    "        </div>-->\n" +
    "      </div>\n" +
    "      <div class=\"row\">\n" +
    "        <div class=\"col-md-8 col-md-offset-2\"><a href=\"http://my.reddonor.com/signup\" class=\"regbtn\">Sign Up For An Account</a></div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/index.html',
    "<!DOCTYPE html>\n" +
    "<html lang=\"en\" data-ng-app=\"reddonor\">\n" +
    "  <head>\n" +
    "    <!--title Red Donor-->\n" +
    "    <title data-ng-bind=\"page.title\" ng-cloak class=\"ng-cloak\">Red Donor</title>\n" +
    "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
    "    <meta name=\"title\" content=\"Red Donor: Connecting Those Who Need Blood To Blood Donors In Your Community\">\n" +
    "    <meta name=\"description\" content=\"Red Donor makes it easier for those who need blood to connect with blood donors in their community.\">\n" +
    "    <meta name=\"keywords\" content=\"Blood Donations, Blood Donors, Caribbean Blood Donation, Caribbean Blood Donation Centres\">\n" +
    "    <meta property=\"og:title\" content=\"Red Donor: Connecting Those Who Need Blood To Blood Donors In Your Community\">\n" +
    "    <meta property=\"og:description\" content=\"Red Donor makes it easier for those who need blood to connect with blood donors in their community.\">\n" +
    "    <meta property=\"og:type\" content=\"website\">\n" +
    "    <meta property=\"og:url\" content=\"http://www.reddonor.com\">\n" +
    "    <meta property=\"og:site_name\" content=\"Red Donor\">\n" +
    "    <!-- Bootstrap -->\n" +
    "    <!--link(href='/vendor/bootstrap/css/bootstrap.min.css', rel='stylesheet', media='screen')-->\n" +
    "    <!-- Included CSS Files-->\n" +
    "    <link href=\"/styles/styles.min.css\" rel=\"stylesheet\" media=\"screen\">\n" +
    "    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries --><!--[if lt IE 9]>\n" +
    "    <script src=\"/vendor/bootstrap/assets/js/html5shiv.js\"></script>\n" +
    "    <script src=\"/vendor/bootstrap/assets/js/respond.min.js\"></script><![endif]--><!--\n" +
    "    <Included>Web Fonts</Included>-->\n" +
    "    <link rel=\"stylesheet\" href=\"http://fonts.googleapis.com/css?family=Open+Sans:400,600\">\n" +
    "    <link rel=\"stylesheet\" href=\"http://fonts.googleapis.com/css?family=Varela+Round\">\n" +
    "    <link rel=\"stylesheet\" href=\"http://fonts.googleapis.com/css?family=Dosis:500\">\n" +
    "    <link rel=\"stylesheet\" href=\"http://fonts.googleapis.com/css?family=Lato\">\n" +
    "    <link rel=\"stylesheet\" href=\"http://fonts.googleapis.com/css?family=Arvo:400,700\">\n" +
    "    <link rel=\"stylesheet\" href=\"//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css\">\n" +
    "    <link rel=\"shortcut icon\" href=\"/assets/images/favicon.ico\">\n" +
    "    <script type=\"text/javascript\">\n" +
    "      var _gaq = _gaq || [];\n" +
    "      _gaq.push(['_setAccount', 'UA-37326435-1']);\n" +
    "      _gaq.push(['_setDomainName', 'reddonor.com']);\n" +
    "      _gaq.push(['_trackPageview']);\n" +
    "      (function() {\n" +
    "        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;\n" +
    "        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';\n" +
    "        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);\n" +
    "      })();\n" +
    "      \n" +
    "    </script>\n" +
    "  </head>\n" +
    "  <body>\n" +
    "    <div id=\"fb-root\"></div>\n" +
    "    <script type=\"text/javascript\">\n" +
    "      (function(d, s, id) {\n" +
    "      var js, fjs = d.getElementsByTagName(s)[0];\n" +
    "      if (d.getElementById(id)) return;\n" +
    "      js = d.createElement(s); js.id = id;\n" +
    "      js.src = \"//connect.facebook.net/en_US/all.js#xfbml=1&appId=222723707782303\";\n" +
    "      fjs.parentNode.insertBefore(js, fjs);\n" +
    "      }(document, 'script', 'facebook-jssdk'));\n" +
    "      \n" +
    "    </script>\n" +
    "    <!--#wrap-->\n" +
    "    <!-- Fixed Navigation Bar ================================================== -->\n" +
    "    <div role=\"navigation\" class=\"navbar navbar-default navbar-fixed-top\">\n" +
    "      <div class=\"container\">\n" +
    "        <div class=\"navbar-header\">\n" +
    "          <button type=\"button\" data-toggle=\"collapse\" data-target=\".navbar-collapse\" class=\"navbar-toggle\"><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span></button><a href=\"/\" class=\"navbar-brand\"><img src=\"/assets/images/red-donor-logo.png\"></a>\n" +
    "        </div>\n" +
    "        <div class=\"collapse navbar-collapse\">\n" +
    "          <ul class=\"nav navbar-nav navbar-right\">\n" +
    "            <!--li: a(href='/donate') Donate-->\n" +
    "            <li><a href=\"/aboutblood\">About Blood</a></li>\n" +
    "            <li><a href=\"/giveblood\">Giving Blood</a></li>\n" +
    "            <li><a href=\"/about\">About Us</a></li>\n" +
    "            <li><a href=\"http://my.reddonor.com\" class=\"menubtn\">Login / Signup</a></li>\n" +
    "          </ul>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "    <!-- Main Content-->\n" +
    "    <div ng-view><!--\n" +
    "      <Footer>(Sticky) ================================================== </Footer>-->\n" +
    "    </div>\n" +
    "    <div id=\"footer\">\n" +
    "      <div class=\"container footer\">\n" +
    "        <div class=\"row\">\n" +
    "          <div class=\"col-sm-3 footer-info\"><a href=\"/\"><img src=\"/assets/images/red-donor-logo.png\"></a>\n" +
    "            <p>Red Donor is focused on increasing the amount of blood available to communities for medical treatment and disaster relief.</p>\n" +
    "          </div>\n" +
    "          <div class=\"col-sm-2 col-xs-4\">\n" +
    "            <h4>About Us</h4>\n" +
    "            <ul>\n" +
    "              <li><a href=\"/about\">Our Story</a></li>\n" +
    "              <li><a href=\"/howitworks\">How It Works</a></li><!--li<a href=\"/partners\">Partners</a>--><!--li<a href=\"/advertise\">Advertising          </a>-->\n" +
    "            </ul>\n" +
    "          </div>\n" +
    "          <div class=\"col-sm-2 col-xs-4\">\n" +
    "            <h4>Get Invovled</h4>\n" +
    "            <ul><!--li<a href=\"/donate\">Donate</a>-->\n" +
    "              <li><a href=\"/faqs\">FAQs</a></li>\n" +
    "              <li><a href=\"/contact\">Contact</a></li>\n" +
    "            </ul>\n" +
    "          </div>\n" +
    "          <div class=\"col-sm-2 col-xs-4\">\n" +
    "            <h4>Connect</h4>\n" +
    "            <ul>\n" +
    "              <li><a href=\"http://blog.reddonor.com\" target=\"_blank\">News</a></li>\n" +
    "              <li><a href=\"https://www.facebook.com/RedDonor\" target=\"_blank\">Facebook</a></li>\n" +
    "              <li><a href=\"https://twitter.com/reddonor\" target=\"_blank\">Twitter</a></li>\n" +
    "            </ul>\n" +
    "          </div>\n" +
    "          <div class=\"col-sm-3 hidden-xs\">\n" +
    "            <div id=\"likebox-frame\">\n" +
    "              <div data-href=\"https://www.facebook.com/RedDonor\" data-width=\"292\" data-colorscheme=\"dark\" data-show-faces=\"true\" data-header=\"false\" data-stream=\"false\" data-show-border=\"false\" class=\"fb-like-box\"></div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "    <div class=\"footer-bottom\">\n" +
    "      <div class=\"container\">\n" +
    "        <div class=\"row\">\n" +
    "          <ul>\n" +
    "            <li><a href=\"/\">© 2013 <span>Red Donor</span></a></li>\n" +
    "            <li><a href=\"/termsofuse\">Terms of Use</a></li>\n" +
    "            <li><a href=\"/privacy\">Privacy Policy  </a></li>\n" +
    "          </ul>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->\n" +
    "    <script src=\"//code.jquery.com/jquery.js\"></script>\n" +
    "    <!-- AngularJs-->\n" +
    "    <script src=\"https://ajax.googleapis.com/ajax/libs/angularjs/1.0.8/angular.min.js\"></script>\n" +
    "    <script src=\"/assets/js/app.min.js\"></script>\n" +
    "    <!-- Include all compiled plugins (below), or include individual files as needed -->\n" +
    "    <!--script(src='vendor/bootstrap/js/bootstrap.min.js')-->\n" +
    "    <!--script(src='vendor/holder/holder.min.js')-->\n" +
    "  </body>\n" +
    "</html>"
  );


  $templateCache.put('views/partials/templates/privacy.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>Privacy Policy</h1>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <p>This Privacy Policy governs the manner in which Red Donor collects, uses, maintains and discloses information collected from users (each, a \"User\") of the www.reddonor.com website (\"Site\"). This privacy policy applies to the Site and all products and services offered by Red Donor.</p>\n" +
    "      <h4>Personal identification information</h4>\n" +
    "      <p>We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the site, subscribe to the newsletter, fill out a form, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, name, email address. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</p>\n" +
    "      <h4>Non-personal identification information</h4>\n" +
    "      <p>We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.</p>\n" +
    "      <h4>Web browser cookies</h4>\n" +
    "      <p>Our Site may use \"cookies\" to enhance User experience. User's web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>\n" +
    "      <h4>How we use collected information</h4>\n" +
    "      <p>Red Donor may collect and use Users personal information for the following purposes:</p>\n" +
    "      <ul>\n" +
    "        <li>To improve customer service\n" +
    "          <p>Information you provide helps us respond to your customer service requests and support needs more efficiently.</p>\n" +
    "        </li>\n" +
    "        <li>To personalize user experience\n" +
    "          <p>We may use information in the aggregate to understand how our Users as a group use the services and resources provided on our Site.</p>\n" +
    "        </li>\n" +
    "        <li>To improve our Site\n" +
    "          <p>We may use feedback you provide to improve our products and services.</p>\n" +
    "        </li>\n" +
    "        <li>To run a promotion, contest, survey or other Site feature\n" +
    "          <p>To send Users information they agreed to receive about topics we think will be of interest to them.</p>\n" +
    "        </li>\n" +
    "        <li>To send periodic emails</li>\n" +
    "      </ul>\n" +
    "      <p>We may use the email address to respond to their inquiries, questions, and/or other requests. If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email or User may contact us via our Site.</p>\n" +
    "      <h4>How we protect your information</h4>\n" +
    "      <p>We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.</p>\n" +
    "      <h4>Sharing your personal information</h4>\n" +
    "      <p>We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above. We may use third party service providers to help us operate our business and the Site or administer activities on our behalf, such as sending out newsletters or surveys. We may share your information with these third parties for those limited purposes provided that you have given us your permission.</p>\n" +
    "      <h4>Compliance with children's online privacy protection act</h4>\n" +
    "      <p>Protecting the privacy of the very young is especially important. For that reason, we never collect or maintain information at our Site from those we actually know are under 13, and no part of our website is structured to attract anyone under 13.</p>\n" +
    "      <h4>Changes to this privacy policy</h4>\n" +
    "      <p>Red Donor has the discretion to update this privacy policy at any time. When we do, we will revise the updated date at the bottom of this page. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>\n" +
    "      <h4>Your acceptance of these terms</h4>\n" +
    "      <p>By using this Site, you signify your acceptance of this policy. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes. </p>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/procedure.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>Give Blood</h1>\n" +
    "        <h2>Blood is a vital lifeline to saving and preserving hundreds of lives daily. Less than 2% of the population donates blood.</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row\">\n" +
    "    <givenav></givenav>\n" +
    "  </div>\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h2 class=\"title\">THE DONATION PROCEDURE</h2>\n" +
    "      <div class=\"introduction\">\n" +
    "        <h2>Giving blood is a simple act of kindness. You can play a vital role in providing blood to save lives.</h2>\n" +
    "      </div>\n" +
    "      <h4>BEFORE DONATING</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>The day before you donate, drink plenty of liquids</p>\n" +
    "        <p>Eat something at least 3 hours before you donate</p>\n" +
    "        <p>Wear clothing with sleeves that can be rolled up above the elbow</p>\n" +
    "        <p>Bring a form of identification</p>\n" +
    "      </div>\n" +
    "      <h4>DURING THE DONATION</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Before donation you will be asked a few questions to determine if you are eligible to donate blood and asked to show your identification.</p>\n" +
    "        <p>Your answers to the interview questions will be private and confidential.</p>\n" +
    "        <p>Your hemoglobin level, temperature, pulse and blood will be checked before donation.</p>\n" +
    "        <p>An area on your arm will be cleaned and a sterile needle will be inserted.</p>\n" +
    "        <p>The blood collection process will last about 10 minutes, until a pint of blood has been collected.</p>\n" +
    "        <p>After donating, you will be asked to rest in the chair for a few minutes during which you can request a drink.</p>\n" +
    "      </div>\n" +
    "      <h4>AFTER DONATING</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Continue to drink water throughout the day.</p>\n" +
    "        <p>Try not to over exert yourself for the rest of the day.</p>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/terms.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>Terms and Conditions of Use</h1>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h4>1. Terms</h4>\n" +
    "      <p>These terms and conditions govern your use of this website; by using this website, you accept these \n" +
    "        <terms>and conditions in full and without reservation. If you disagree with these terms and conditions or </terms>\n" +
    "        <any>part of these terms and conditions, you must not use this website.</any>\n" +
    "      </p>\n" +
    "      <h4>2. Use License</h4>\n" +
    "      <p>Unless otherwise stated, Red Donor owns the intellectual property rights published on this website and materials used on Red Donor. Subject to the license below, all these intellectual property rights are reserved.</p>\n" +
    "      <p>You may view, download for caching purposes only, and print pages, files or other content from the website for your own personal use, \n" +
    "        <subject>to the restrictions set out below and elsewhere in these terms and conditions.</subject>\n" +
    "      </p>\n" +
    "      <p>You must not: </p>\n" +
    "      <ul>\n" +
    "        <li>republish material from this website in neither print nor digital media or documents (including republication on another website);</li>\n" +
    "        <li>sell, rent or sub-license material from the website;</li>\n" +
    "        <li>show any material from the website in public;</li>\n" +
    "        <li>reproduce, duplicate, copy or otherwise exploit material on this website for a commercial purpose;</li>\n" +
    "        <li>edit or otherwise modify any material on the website;</li>\n" +
    "        <li>redistribute material from this website - except for content specifically and expressly made available for redistribution; or</li>\n" +
    "        <li>republish or reproduce any part of this website through the use of iframes or screenscrapers.</li>\n" +
    "      </ul>\n" +
    "      <p>Where content is specifically made available for redistribution, it may only be redistributed within your organisation.</p>\n" +
    "      <h4>3. Acceptable Use</h4>\n" +
    "      <p>\n" +
    "        You must not use this website in any way that causes, or may cause, damage to the website or impairment of the availability \n" +
    "        or accessibility of Red Donor or in any way which is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity.\n" +
    "      </p>\n" +
    "      <p>\n" +
    "        You must not use this website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, \n" +
    "        worm, keystroke logger, rootkit or other malicious computer software.\n" +
    "      </p>\n" +
    "      <p>You must not conduct any systematic or automated data collection activities on or in relation to this website without Red Donor’s express written consent.</p>\n" +
    "      <p>This includes:</p>\n" +
    "      <ul>\n" +
    "        <li>scraping</li>\n" +
    "        <li>data mining</li>\n" +
    "        <li>data extraction</li>\n" +
    "        <li>data harvesting</li>\n" +
    "        <li>'framing' (iframes)</li>\n" +
    "      </ul>\n" +
    "      <p>You must not use this website or any part of it to transmit or send unsolicited commercial communications.</p>\n" +
    "      <p>You must not use this website for any purposes related to marketing without the express written consent of Red Donor.com.</p>\n" +
    "      <h4>4. Restricted Access</h4>\n" +
    "      <p>\n" +
    "        Access to certain areas of this website is restricted. www.reddonor.com reserves the right to restrict access to certain areas of this website, \n" +
    "        or at our discretion, this entire website. www.reddonor.com may change or modify this policy without notice.\n" +
    "      </p>\n" +
    "      <p>\n" +
    "        If www.reddonor.com provides you with a user ID and password to enable you to access restricted areas of this website or other content or services, you must ensure that the \n" +
    "        user ID and password are kept confidential. You alone are responsible for your password and user ID security.\n" +
    "      </p>\n" +
    "      <p>Red Donor.com may disable your user ID and password at www.reddonor.com's sole discretion without notice or explanation.</p>\n" +
    "      <h4>5. User Content</h4>\n" +
    "      <p>In these terms and conditions, “your user content” means material (including without limitation text, images, audio material, video material and audio-visual material) that you submit to this website, for whatever purpose.</p>\n" +
    "      <p>You grant to Red Donor a worldwide, irrevocable, non-exclusive, royalty-free license to use, reproduce, adapt, publish, translate and distribute your user content in any existing or future media. You also grant to Red Donor the right to sub-license these rights, and the right to bring an action for infringement of these rights.</p>\n" +
    "      <p>Your user content must not be illegal or unlawful, must not infringe any third party's legal rights, and must not be capable of giving rise to legal action whether against you or Red Donor or a third party (in each case under any applicable law).</p>\n" +
    "      <p>You must not submit any user content to the website that is or has ever been the subject of any threatened or actual legal proceedings or other similar complaint.</p>\n" +
    "      <p>Red Donor reserves the right to edit or remove any material submitted to this website, or stored on the servers of Red Donor, or hosted or published upon this website.</p>\n" +
    "      <p>Red Donor's rights under these terms and conditions in relation to user content, Red Donor does not undertake to monitor the submission of such content to, or the publication of such content on, this website.</p>\n" +
    "      <h4>6. No Warranties</h4>\n" +
    "      <p>This website is provided “as is” without any representations or warranties, express or implied. www.reddonor.com makes no representations or warranties in relation to this website or the information and materials provided on this website.</p>\n" +
    "      <p>Without prejudice to the generality of the foregoing paragraph, Red Donor does not warrant that:</p>\n" +
    "      <ul> \n" +
    "        <li>this website will be constantly available, or available at all; or</li>\n" +
    "        <li>the information on this website is complete, true, accurate or non-misleading.</li>\n" +
    "      </ul>\n" +
    "      <p>Nothing on this website constitutes, or is meant to constitute, advice of any kind. If you require advice in relation to any legal, financial or medical matter you should consult an appropriate professional.</p>\n" +
    "      <h4>7. Limitations of Liability</h4>\n" +
    "      <p>Red Donor will not be liable to you (whether under the law of contact, the law of torts or otherwise) in relation to the contents of, or use of, or otherwise in connection with, this website:</p>\n" +
    "      <ul> \n" +
    "        <li>to the extent that the website is provided free-of-charge, for any direct loss;</li>\n" +
    "        <li>for any indirect, special or consequential loss; or</li>\n" +
    "        <li>for any business losses, loss of revenue, income, profits or anticipated savings, loss of contracts or business relationships, loss of reputation or goodwill, or loss or corruption of information or data.</li>\n" +
    "      </ul>\n" +
    "      <p>These limitations of liability apply even if Red Donor has been expressly advised of the potential loss.</p>\n" +
    "      <h4>8. Indemnity</h4>\n" +
    "      <p>You hereby indemnify Red Donor and undertake to keep Red Donor indemnified against any losses, damages, costs, liabilities and expenses (including without limitation legal expenses and any amounts paid by www.reddonor.com to a third party in settlement of a claim or dispute on the advice of Red Donor‘s legal advisers) incurred or suffered by Red Donor arising out of any breach by you of any provision of these terms and conditions, or arising out of any claim that you have breached any provision of these terms and conditions.</p>\n" +
    "      <h4>9.\tBreaches of these terms and conditions</h4>\n" +
    "      <p>Without prejudice to Red Donor's other rights under these terms and conditions, if you breach these terms and conditions in any way, www.reddonor.com may take such action as www.reddonor.com deems appropriate to deal with the breach, including suspending your access to the website, prohibiting you from accessing the website, blocking computers using your IP address from accessing the website, contacting your internet service provider to request that they block your access to the website and/or bringing court proceedings against you.</p>\n" +
    "      <h4>10.\tVariation</h4>\n" +
    "      <p>Red Donor may revise these terms and conditions from time-to-time. Revised terms and conditions will apply to the use of this website from the date of the publication of the revised terms and conditions on this website. Please check this page regularly to ensure you are familiar with the current version.</p>\n" +
    "      <h4>11.\tEntire agreement</h4>\n" +
    "      <p>These terms and conditions, together with Red Donor‘s Privacy Policy constitute the entire agreement between you and Red Donor in relation to your use of this website, and supersede all previous agreements in respect of your use of this website.</p>\n" +
    "      <p>This document was last updated on December 08, 2012</p>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/tips.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>Give Blood</h1>\n" +
    "        <h2>Blood is a vital lifeline to saving and preserving hundreds of lives daily. Less than 2% of the population donates blood.</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row\">\n" +
    "    <givenav></givenav>\n" +
    "  </div>\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h2 class=\"title\">DONATION TIPS</h2>\n" +
    "      <div class=\"introduction\">\n" +
    "        <h2>For a safe and successful blood donation, follow these preparations, precautions and recommended tips.</h2>\n" +
    "      </div>\n" +
    "      <h4>BEFORE YOUR DONATION</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Get a good night’s rest. </p>\n" +
    "        <p>Drink plenty of water or non-alcoholic beverages before the donation. Drinking water or fruit juices helps keep your blood pressure up.</p>\n" +
    "        <p>Eat a healthy meal 3 hours before your donation. Eating will keep your blood sugar levels stable and ward off possible dizziness after donation.</p>\n" +
    "        <p>Avoid eating fatty foods before your donation. An increase of fat in your blood stream might make testing of your blood difficult.</p>\n" +
    "        <p>Have your identification card with you at the blood donation centre.</p>\n" +
    "      </div>\n" +
    "      <h4>DURING YOUR DONATION</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Wear clothing with sleeves that can be raised above the elbow.</p>\n" +
    "        <p>Relax, listen to music, read or talk to other donors. Nervousness can cause your blood pressure to drop and can lead to dizziness.</p>\n" +
    "        <p>Have a juice or snack after the donation. Rest for a few minutes to let your body adjust before continuing your day.</p>\n" +
    "      </div>\n" +
    "      <h4>AFTER YOUR DONATION</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Drink plenty of liquids and avoid alcohol over the next 24 hours.</p>\n" +
    "        <p>Eat a high protein meal after your donation.</p>\n" +
    "        <p>Avoid heavy lifting or vigorous exercise for the rest of the day.</p>\n" +
    "        <p>If you experience dizziness after donation, stop what you’re doing and rest or lie down until you feel better.</p>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );


  $templateCache.put('views/partials/templates/why-donate.html',
    "\n" +
    "<section class=\"sub-header\">\n" +
    "  <div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"sub-text col-md-10 col-md-offset-1\">\n" +
    "        <h1>Give Blood</h1>\n" +
    "        <h2>Blood is a vital lifeline to saving and preserving hundreds of lives daily. Less than 2% of the population donates blood.</h2>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>\n" +
    "<section class=\"container\">\n" +
    "  <div class=\"row\">\n" +
    "    <givenav></givenav>\n" +
    "  </div>\n" +
    "  <div class=\"row page-content\">\n" +
    "    <div class=\"col-md-10 col-md-offset-1\">\n" +
    "      <h2 class=\"title\">WHY DONATE</h2>\n" +
    "      <div class=\"introduction\">\n" +
    "        <h2>One donation of blood can help to save up to 3 different people.</h2>\n" +
    "      </div>\n" +
    "      <h4>HELPING PEOPLE</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>The blood collected from donations is used for many purposes including:</p>\n" +
    "        <ul>\n" +
    "          <li>Accident Victims</li>\n" +
    "          <li>Emergency Operations</li>\n" +
    "          <li>Organ Transplants</li>\n" +
    "          <li>Leukemia and Cancer Patients</li>\n" +
    "          <li>Cardiac (Heart) Surgeries</li>\n" +
    "          <li>Premature Babies</li>\n" +
    "        </ul>\n" +
    "        <p>Without blood transfusions these patients will not survive. These patients may be a family member, friend or work colleague who may need blood today or one day in the future. Because blood has a limited shelf life, constant donations are needed year round by healthy members of the community.</p>\n" +
    "      </div>\n" +
    "      <h4>HEALTH BENEFITS</h4>\n" +
    "      <div class=\"blurb\">\n" +
    "        <p>Donating blood also has some health benefits. Before each donation you receive a mini physical to check your:</p>\n" +
    "        <ul>\n" +
    "          <li>Pulse</li>\n" +
    "          <li>Blood Pressure</li>\n" +
    "          <li>Body Temperature</li>\n" +
    "          <li>Haemoglobin</li>\n" +
    "        </ul>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section>"
  );

}]);
